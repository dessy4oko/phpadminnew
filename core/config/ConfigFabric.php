<?php 
namespace App;
require_once dirname(__FILE__) . '/Config.php';

final class ConfigLoadException extends \Exception {
    private string $requestedPath = '';
    public function __construct(string $path, string $message, int $code = 0, \Throwable $previous = null) {
        $this->requestedPath = $path;
        parent::__construct($message, $code, $previous);
    }
    public function getPath(): string {
        return $this->requestedPath;
    }
}
final class InvalidJsonConfig extends \Exception {
    private string $requestedPath = '';
    public function __construct(string $path, string $message, int $code = 0, \Throwable $previous = null) {
        $this->requestedPath = $path;
        parent::__construct($message, $code, $previous);
    }
    public function getPath(): string {
        return $this->requestedPath;
    }
}

final class InvalidConfigKey extends \Exception {
    private string $key = '';
    public function __construct(string $key, string $message, int $code = 0, \Throwable $previous = null) {
        $this->key = $key;
        parent::__construct($message, $code, $previous);
    }
    public function getKey() : string {
        return $this->key;
    }
}
/**
 * @throws \App\ConfigLoadException
 * @throws \App\EmptyConfigObject
 */
abstract class ConfigFabric {

    protected static array $config = [];
    protected static string $defaultKey = '';

    /**
     * First execution of this method will set passed key as default key. If key will not be passed
     * then default key will have value of 'default'
     * @param string $path Config file path in json format
     * @param string $key='default' Key of configuration object
     * 
     * @throws ConfigLoadException|InvalidJsonConfig When config file does not exists
     * @throws 
     * 
     * @return null;
     */
    public static function load(string $path, string $key = 'default')
    {
        if(!file_exists($path)) {
            throw new ConfigLoadException($path, "Invalid config path passed to ".__METHOD__." -- looked into {$path}");
        }
        $props = json_decode(file_get_contents($path));

        if(is_null($props)) {
            throw new InvalidJsonConfig($path, "Invalid json readed from config file -- loaded from {$path}");
        }

        self::store($props, $key);
    }

    /**
     * Stores config object
     */
    public static function store(object $props, string $key = 'default') {
        if(empty(self::$config)) self::$defaultKey = $key;
        self::$config[$key] = new Config($props);
    }
    /**
     * Returns default config key
     * @return string
     */
    public static function getDefaultKey(): string {
        return self::$defaultKey;
    }

    public static function setDefaultKey(string $key) : void {
        self::$defaultKey = $key;
    }

    /**
     * Returns App configuration object with specified key.
     * Returns default config object (first loaded) in case if key is not passed
     * @param string $key=null Key of configuration object
     * 
     * @return Config
     */
    public static function get(string $key = null): Config
    {
        if($key === null) $key = self::$defaultKey;

        if(self::has($key)) {
            return self::$config[$key];
        }
        throw new InvalidConfigKey($key, "Can not resolve configuration with passed key");
    }

    /**
     * Checks that ConfigFabric contains Config object with passed key
     * @param string $key Key of configuration object
     * @return bool
     */
    public static function has(string $key):bool {
        return isset(self::$config[$key]);
    }
}

?>
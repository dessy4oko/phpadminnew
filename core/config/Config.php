<?php 
namespace App;

class ConfigPropertyApsend extends \Exception {

    public function __construct($message, $code = 0, \Throwable $previous = null)
    {
        $message = "Application config has no requested property. " . $message;
        parent::__construct($message, $code, $previous);    
    }
}

final class EmptyConfigObject extends \Exception {
    private string $requestedPath = '';
    public function __construct(string $message = 'Empty config object passed to instance constructor', int $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
    public function getPath(): string {
        return $this->requestedPath;
    }
}


class Config {
    protected object $props;

    public function __construct(object $props)
    {
        if(empty($props) || empty(get_object_vars($props))) throw new EmptyConfigObject(); 
        $this->props = $props;
    }

    /**
     * @param string $name Name of property to get
     * 
     * @throws ConfigPropertyApsend
     * @return mixed
     */
    public function __get(string $name)
    {  
        $keys = array_keys(get_object_vars($this->props));
        if(!in_array($name, $keys)) throw new ConfigPropertyApsend('Config has no property with name '. $name);
        return $this->props->{$name};
    }
}
?>
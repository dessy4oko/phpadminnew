<?php
namespace App;

use App\Application;
use App\Request;
use App\Response;

class db extends \App\MiddleWare
{

    use \App\Subscriber;

    public static ?\PDO $connection = null;

    public function __construct(object $dbconf)
    {
        if(!is_null(self::$connection)) return self::$connection;
        self::$connection = new \PDO($dbconf->driver . ':host=' . $dbconf->host . ';dbname=' . $dbconf->dbname, $dbconf->username, $dbconf->password);
    }

    public function __invoke(Request &$request, Response &$response)
    {
        $app = \App\Application::instance();
        $this->subscribe($app, 'exit', 'close');
    }

    public function close() {
        self::$connection = null;
    }

}

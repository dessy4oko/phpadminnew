<?php 
namespace App;

use PDOStatement;

interface IShema {
  public function getOne(): \PDOStatement;
}

abstract class Shema {

  protected string $tableName;
  protected string $primary;
  protected array $fields = [];

  public function __construct(string $tableName, array $fields)
  {
    $this->tableName = $tableName;
    $this->fields = $fields;
  }

  public function getName(): string {
    return $this->tableName;
  }

  public function getOne(): \PDOStatement {
      return \App\db::$connection->prepare("SELECT * FROM `{$this->tableName}` WHERE {$this->primary} = :value");
  }

  public static function field(string $fieldName, string $type) {

  }

}
?>
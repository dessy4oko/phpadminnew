<?php 
    namespace App;

    trait Errors {

        private array $errors = [];
        
        private array $messages = [];
        
        private array $warnings = [];
        
        /**
         * Check if some errors occured
         * @return bool
         */
        public function hasErrors() : bool {
            return !empty($this->errors);
        }

        /**
         * Check if some errors occured
         * @return bool
         */
        public function hasWarnings() {
            return !empty($this->warnings);
        }

        /**
         * Check if some errors occured
         * @return bool
         */
        public function hasMessages() {
            return !empty($this->messages);
        }

        public function hasAlerts() {
            return $this->hasErrors() || $this->hasWarnings() || $this->hasMessages();
        }

        public function getErrors() {
            return $this->errors;
        }

        public function getMessages() {
            return $this->messages;
        }

        public function getWarnings() {
            return $this->warnings;
        }

        public function getAlerts() {
            return array_merge($this->errors, $this->warnings, $this->messages);
        }

        /**
         * @param string|\Exception $mes;
         */
        private function process_data($mes, $status) {
            if(gettype($mes) === 'object' && method_exists($mes, 'getMessage')) {
                return [
                    'message' => $mes->getMessage(),
                    'status' => $status
                ];
            } elseif(gettype($mes) === 'string') {
                return [
                    'message' => $mes,
                    'status' => $status
                ];
            }
        }
        /**
         * Collects occured error to internal errors array
         * @param $error Errror info
         * @return null
         */
        public function provideError($error) {
            $this->errors[] = $this->process_data($error, 'ERROR');
        }
        /**
         * Collects occured warnings to internal warnings array
         * @param $error Errror info
         * @return null
         */
        public function provideWarning($warning) {
            $this->warnings[] = $this->process_data($warning, 'WARNING');
        }

        /**
         * Collects message to internal messages array
         * @param $error Errror info
         * @return null
         */
        public function provideMessage($message) {
            $this->messages[] = $this->process_data($message, 'SUCCESS');
        }
    }
?>
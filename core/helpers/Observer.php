<?php 
    namespace App;

    trait Observer {
        protected array $subscriptions = [];

        /**
         * Check if Observer has subscribers
         * @param ?string $event Event to check
         */
        public function hasSubscribers(?string $event = null) {
            if(is_null($event)) {
                return empty($this->subscriptions);
            }

            return isset($this->subscriptions[$event]);
        }

        /**
         * Subscribe object on event triggering passing object method as callback.
         * @param string $event Event name to subscribe
         * @param object $object Object which will be subscribed on event
         * @param string $method Method to call when event will be triggered
         * 
         * @throws 
         * 
         * @return void
         */
        public function addSubscriber(string $event,object &$object,string $method) : void {
            if(!method_exists($object, $method)) {
                $classname = gettype($object);
                throw new \App\InvalidObserverCallback($method, "Object with class name {$classname} have not {$method} method.");
            }

            if(!isset($this->subscriptions[$event])) $this->subscriptions[$event] = [];
            $this->subscriptions[$event][] = [$object, $method];
        }

        /**
         * Triggers event and calls object methods
         * @param string $event Event to trigger
         * @param array $data Data of event
         * @return mixed
         */
        public function trigger(string $event, array $data = null) {
            if(!$this->hasSubscribers($event)) {
                return null;
            }
            $retval = null;
            foreach ($this->subscriptions[$event] as $subscription) {
                $retval = \call_user_func_array($subscription, $data !== null ? $data : []);
            }
            return $retval;
        }

    }
?>
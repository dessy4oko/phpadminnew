<?php

namespace App;

trait Subscriber
{

    /**
     * @param object $observer Observer
     * @param $event Event to subscribe
     * @param $method Method to use as event callback
     * @return void
     */
    public function subscribe(object &$observer, string $event, string $method): void
    {
        if(!\App\AllowedObserverType::allowed($observer)) throw new \App\NotAllowedType(get_class($observer), "Can not subscribe on object which do not support subscription.");
        $observer->addSubscriber($event, $this, $method);
    }
}

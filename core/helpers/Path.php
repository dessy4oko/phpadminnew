<?php

namespace App;

define('__ROOT__', dirname(__DIR__) . '/../');
define('__PLLUGINS__', dirname(__DIR__) . '/../infusions/');

abstract class Path
{

    protected static string $base = __ROOT__;
    protected static array $modMap = [
        '+' => __PLLUGINS__,
    ];
    protected static ?string $encoding = null;

    /**
     * Sets inherit encoding
     * @param string $encoding Data encoding
     * @return void
     */
    public static function setEncoding(string $encoding):void {
        self::$encoding = $encoding;
    }
    /**
     * Returns application root directory
     *
     * @return ResolvedPath
     */
    public static function root(): ResolvedPath
    {
        return self::resolve(self::$base);
    }

    /**
     * Check for modificator symbol
     * @param string $path Path to check modificator contains
     * @return
     */
    protected static function hasModificator($path): bool
    {
        return in_array(self::modificator($path), array_merge(['~'], array_keys(self::$modMap)));
    }

    /**
     * Returns modificator of string
     * @param string $path Path to cut modificator
     * @return string Modificator
     */
    protected static function modificator(string $path): string
    {
        if(is_null(self::$encoding)) return substr($path, 0, 1);
        return mb_strcut($path, 0, 1, self::$encoding);
    }

    /**
     * Modify string to system based path
     * @param string $path Path to modify
     */
    protected static function modify(string &$path) : void
    {
        if (!self::hasModificator($path)) {
            return;
        }
        if(is_null(self::$encoding)) {
            $path = self::resolveModificator(self::modificator($path)) . substr($path, 1, strlen($path));
            return;
        }
        $path = self::resolveModificator(self::modificator($path)) . mb_strcut($path, 1, mb_strlen($path, self::$encoding), self::$encoding);
    }

    /**
     * Resolves modificator
     * @param string $modificator Modificator to resolve
     * @return string
     */
    protected static function resolveModificator(string $modificator): string
    {
        if ($modificator === '~') {
            return self::root();
        }

        return self::$modMap[$modificator];
    }

    /**
     * Resolving path depends on system delimeter character
     *
     * @param string $path Path to resolve inside
     *
     * @return ResolvedPath
     */
    public static function resolve(string $path): ResolvedPath
    {

        self::modify($path);

        $path = preg_replace("/\//", DIRECTORY_SEPARATOR, $path);

        $pathSteps = explode(DIRECTORY_SEPARATOR, $path);
        $finalSteps = [];
        foreach ($pathSteps as $index => $step) {
            if ($step === '.') {
                continue;
            }

            if ($step === '..') {array_pop($finalSteps);
                continue;}
            !empty($step) && array_push($finalSteps, $step);
        }

        $path = implode(DIRECTORY_SEPARATOR, $finalSteps);

        return new ResolvedPath($path);
    }
}

class ResolvedPath
{
    protected $path = '';
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function __toString(): string
    {
        return $this->path;
    }

    /**
     * Concatenation with passed values
     * @param string $path, ...
     * 
     * @return ResolvedPath
     */
    public function join(): ResolvedPath
    {
        if(empty(func_get_args())) return $this;
        $this->path = trim($this->path, '/\\') . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, array_map(function($join) {
            return trim($join, '/'.DIRECTORY_SEPARATOR);
        }, func_get_args()));
        return $this;
    }

    /**
     * Resolves current value of object
     * @return \App\ResolvedPath
     */
    public function resolve(): ResolvedPath {
        return \App\Path::resolve((string)$this);
    }

    /**
     * Returns formatted URI without system path.
     * Example: 
     * 
     * $path = A:\OpenServer\domains\newfeatures.local\core\index.php
     * will return /core/index.php
     * 
     * $path = A:\OpenServer\domains\newfeatures.local\core\
     * will return /core/
     *
     * @return string
     */
    public function webpath(): string
    {
        $baseFormatted = "/^".preg_replace("/(\\".DIRECTORY_SEPARATOR.")/", "\/", \App\Path::root()).'/';
        $pathFormatted = preg_replace("/\\".DIRECTORY_SEPARATOR."/", '/', $this->path);
        $path = preg_replace($baseFormatted, '', $pathFormatted);
        $path = preg_replace("/(?!.+\..+)^.+$(?<!\/)/", "$0/", $path);
        return $path;
    }

}
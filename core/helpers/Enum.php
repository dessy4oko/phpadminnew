<?php 
  namespace App;

  abstract class Enum {

    protected static $constCache = [];

    private function __construct() {}
    /**
     * Check that value contained inside enum
     * 
     * @param $value Value to check
     * 
     * @return bool
     */
    public static function has($value): bool {
      return in_array($value, static::fields());
    }

    /**
     * Returns constants values of enum
     * 
     * @return array
     */
    public static function fields(): array {
      if(empty(static::$constCache[get_called_class()])) {
        $reflect = new \ReflectionClass(get_called_class());
        static::$constCache[get_called_class()] = $reflect->getConstants();
      }

      return static::$constCache[get_called_class()];
    }
  }
?>
<?php 
namespace App;

abstract class FilesFabric {

  /**
   * Creates file object depends on its mime type.
   * Allowed mime types presented at /core/enums/AllowedMimeTypes.
   * If type is not compatible with allowed types then NotAllowedType exception will be thrown
   * @throws \App\NotAllowedType|\Exception
   * @return \App\File
   */
  public static function produce(string $type, string $path = null, int $size = null): \App\File {
    if(!AllowedMimeTypes::allowed($type)) {
      throw new NotAllowedType($type, "Can not create file object because passed mime type is not valid for this fabric.");
    }
    switch($type) {
      // JPEG, PNG image
      case AllowedMimeTypes::jpeg:
      case AllowedMimeTypes::png:
        return new ImageFile($path, $type, $size);
      // SVG
      case AllowedMimeTypes::svg:
        return new SvgFile($path, $type, $size);
      default:
        throw new NotAllowedType($type, "Can not create file object because passed mime type is not valid for this fabric.");
    }
  }
}
?>
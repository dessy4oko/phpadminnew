<?php
namespace App;

use Exception;
use finfo;

interface IFile {
  public function __construct(string $path = null, string $type = null, int $size = null);
  public function moveTo(string $path):bool;
  public function put();
  public function read(int $bytesize = null);
  public function rename(string $name): bool;

  public function delete();
  public function copy();
}

trait TFile {
  protected $fhandler = null;
  protected string $name = '';
  protected ?string $path;
  protected ?string $size;
  protected ?string $type;
  /**
   * Common file constructor
   * @param string $name Filename
   * @param string $path Path to file location
   * @param int $size Size of file
   * @param string $type Filetype
   */
  public function fillProperties(string $path, int $size, string $type) {
    $this->path = $path;
    $this->name = basename($this->path);
    $this->size = $size;
    $this->type = $type;
  }

  /**
   * Opens file located at path which passed to file object constructor
   * @return void
   */
  public function open($mode = 'rb'): void {
    if(!is_null($this->fhandler)) return;
    $this->fhandler = fopen($this->path, $mode);
  }

  /**
   * Returns filename
   * @return string
   */
  public function name():string {
    return $this->name;
  }
  /**
   * Returns file location
   * @return string
   */
  public function path():string {
    return $this->path;
  }
  /**
   * Returns file size
   * @return int
   */
  public function size():int {
    return $this->size;
  }
  /**
   * Returns type of file compatible with AllowedMimeTypes
   * @return string
   */
  public function type():string {
    return $this->type;
  }
}

class File implements IFile {
  use TFile;
  /**
   * Creates temp file or read existing file located in *$path*
   * @param string $path File path
   * @param int $size File size. Null as default
   * @param string $type File mimetype
   * @throws \Exception
   */
  public function __construct(string $path = null, string $type = null, int $size = null) {

    // ? If path have not been passed, then creating temporary file
    if(is_null($path)) {
      // * Create temp file
      $this->fhandler = tmpfile();

      // ! THROW Exception 
      // ! If file have not been created in some case
      if(!$this->fhandler) throw new \Exception('Failed to create temporary file');

      // * Get meta data of temporary file and store it in properties
      $meta = stream_get_meta_data($this->fhandler);

      $this->type = $type ?? $meta['wrapper_type'];
      $this->name = basename($meta['uri']);
      $this->path = (string) Path::resolve(sys_get_temp_dir())->join($this->name);
      $this->size = 0;

      return;
    }

    // ! THROW Exception
    // ! If file at specified *$path* location does not exists 
    if(!file_exists($path)) throw new \Exception('File does not exist at ' . $path);

    $path = $path;
    $type = $type ?? mime_content_type($path);
    $size = $size ?? filesize($path);

    $this->fillProperties($path, $size, $type);
  }
  public function put() {}
  public function copy() {}

  /**
   * Renames file and leaves it in the same directory where it is
   * @param string $name New name of file
   * @return bool
   */
  public function rename(string $name): bool {

    if(is_dir($name)) {
      throw new Exception("Invalid filename passed to ".__METHOD__);
    }

    if(is_uploaded_file($this->path)) {
      $this->name = $name;
      return true;
    }

    $filedir = dirname($this->path);

    if(!is_dir($filedir)) {
      throw new \Exception("Unexpected error occured while renaming file process have been evaluating (888).", 888);
    }
    $newfilepath = Path::resolve($filedir)->join($name);

    if(file_exists($newfilepath)) {
      throw new AlreadyExists("Can not rename file in case that file directory already contains file with target filename.");
    }

    $res = rename($this->path, $newfilepath);
    $res && ($this->path = $newfilepath) && ($this->name = $name);

    return $res;
  }
  
  /**
   * Delete file
   * @return bool
   */
  public function delete():bool {
    return unlink($this->path);
  }

  /**
   * Moves file to new location
   * @param string $path Directory to put file
   * @throws \Exception In case if operation has failed
   * @return bool
   */
  public function moveTo(string $path):bool {
    // ! Throw Exception
    // ! If passed path is not valid directory path
    if(!is_dir($path)) throw new \Exception("Invalid path passed to ".__METHOD__);
    
    // Construct new file path
    $newfilepath = Path::resolve($path)->join($this->name);

    // ! THROW \App\AlreadyExists exception
    // ! In case if target folder already contains file with same filename
    if(file_exists((string) $newfilepath)) {
      throw new AlreadyExists("Can not move file to `{$newfilepath->webpath()}`. Target folder already contains file with same filename.");
    }

    // Move file
    if(is_uploaded_file($this->path)) {
      $res = move_uploaded_file($this->path, $newfilepath);
    } else {
      $res = rename($this->path, $newfilepath);
    }

    $res && ($this->path = $newfilepath);

    return $res;
  }

  /**
   * Read file. If file is empty *false* will be returned.
   * @param int $bytesize Size of bytes sector to read. Optional.
   * If not specified then all file will be red.
   * @throws \Exception If file read process fails.
   * @return string
   */
  public function read(int $bytesize = null) {
    if($this->size == 0) return false;
    $this->open("rb");
    
    if(is_null($bytesize)) {
      $contents = '';
      while(!feof($this->fhandler)) {
        $bytes = fread($this->fhandler, 1024);
        if(!$bytes) throw new \Exception('File read process failed');
        $contents .= $bytes;
      }
      return $contents;
    }

    $bytes = fread($this->fhandler, $bytesize);
    if(!$bytes) throw new \Exception('File read process failed');
    return $bytes;
  }

  /**
   * Checks if end of file reached
   * @return bool
   */
  public function eof(): bool {
    return feof($this->fhandler);
  }
}

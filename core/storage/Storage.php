<?php 
  namespace App;

use Exception;

abstract class Storage {

    /**
     * Checks if given path is file
     * @param string $path File path
     * 
     * @return bool
     */
    public static function contains(string $path):bool {
      return file_exists($path) || is_dir($path);
    }

    /**
     * Creates folder with passed foldername located in passed path
     * @param string $foldername Name of folder
     * @param string $path Target directory to create folder
     * @param bool $recurcive 
     * @throws \App\AlreadyExists In case if target folder already contains directory with passed name
     * @throws \Exception In case if passed *$path* is invalid or folder creation process fails
     * @return string
     */
    public static function makefolder(string $foldername, string $path, bool $recurcive = false): string {
      // TODO: create $foldername does not contains modificators test
      // TODO: create recurcive logic of directory creation
      if(!is_dir($path)) {
        throw new \Exception('Can not create folder in specified location. Invalid target path passed to '.__METHOD__);
      }
      if(empty($foldername)) {
        throw new \Exception('Can not create folder with empty foldername');
      }
      $foldername = trim($foldername, "/\\");
      if(preg_match("/[^\w]/", $foldername)) {
        throw new \Exception('Folder name is invalid');
      }
      $base = basename($foldername);
      $target = (string) Path::resolve($path)->join($base);
      if(is_dir($target)) {
        throw new \App\AlreadyExists('Target folder already exists');
      }
      if(!mkdir($target)) {
        throw new \Exception('Failed to create folder');
      }
      
      return $target;
    }

    /**
     * Returns object with interface for working with temporary files.
     * If $path is not passed it will create temporary file in writing mode
     * 
     * @param string $path Path to temporary file. Do not pass this param if you would like to create temporary file.
     * @throws \Exception In case if $path is passed and temporary file does not exists or if creation of temporary file has failed
     * @return IFile
     */
    public static function temporary() : IFile {
      return new class() implements IFile {
        use TFile;
        public function __construct()
        {
          $this->fhandler = tmpfile();
          if(!$this->fhandler) throw new \Exception('Failed to create temporary file');
          return;
        }
      };
    }

    /**
     * Writes contents to file located by given path
     * @param string $path File path
     * @param string $contents Content to write
     * 
     * @throws \Exception
     * @return bool
     */
    public static function put(string $path, string $contents):bool {
      $filehandler = fopen($path, 'w+');
      if(!$filehandler) throw new \Exception('Unable to open/create file in '.$path);
      $res = fwrite($filehandler, $contents);
      if (!fclose($res)) throw new \Exception('Unable to close filehandler opened for '.$path);
      return !!$res;
    }
  }
?>
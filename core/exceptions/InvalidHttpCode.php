<?php

namespace App;

class InvalidHttpCode extends \Exception
{
    protected string $passedCode = '';
    public function __construct(string $passedCode, string $message = '', int $code = 0, \Throwable $previous = null)
    {
        $this->passedCode = $passedCode;
        parent::__construct($message, $code, $previous);
    }

    public function getPassedCode() {
        return $this->passedCode;
    }
}

<?php 

    namespace App;

    class InvalidObserverCallback extends \Exception {
        protected string $callbackName = '';
        public function __construct(string $callback, string $message = '', int $code = 0, \Throwable $previous = null)
        {
            $this->callbackName = $callback;
            parent::__construct($message, $code, $previous);
        }
    }

?>
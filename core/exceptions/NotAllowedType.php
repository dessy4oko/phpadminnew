<?php 
namespace App;

class NotAllowedType extends \Exception {
  protected string $passedType = '';
  public function __construct(string $passedType, string $message = '', int $code = 0, \Throwable $previous = null)
  {
    $this->passedType = $passedType;
    parent::__construct($message, $code, $previous);
  }
  public function passedType():string {
    return $this->passedType;
  }
}
?>
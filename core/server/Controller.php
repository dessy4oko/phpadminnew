<?php 
namespace App;

interface IController {
  public function init():void;
}

abstract class Controller implements IController {

  protected array $routes = [];

  public function __construct() {}

  /**
   * Adds route to controller wich will be triggered when *$pattern* matches with requested URI
   * and request has *$method* type.
   * For example if you define Controller->route('POST', "api/", ...) it will be triggered
   * when user requests http://domain.name/api/ by POST method.
   * 
   * @param string $method Request method defined in \App\RequestMethods
   * @param string $pattern Pattern of URI to match
   * @param \App\MiddleWareSet Additional pre-handlers of request
   * @param \App\RequestHandler $handler Handler of request
   * 
   * @return void 
   */
  public function route(string $method, string $pattern, \App\RequestHandler $handler, \App\MiddleWareSet $middleWareSet = null) {
    if(!\App\RequestMethods::has($method)) throw new \Exception('Undfined request method passed'.get_called_class());
    if(!isset($this->routes[$method])) $this->routes[$method] = [];
    !is_null($middleWareSet) && $handler->use($middleWareSet);
    $this->routes[$method][$pattern] = $handler;
  }


  /**
   * Add GET route to controller. This method is same thing as Controller->route(\App\RequestMethods::get, ..., ...);
   * @param string $pattern Pattern of URI to match
   * @param \App\MiddleWareSet Additional pre-handlers of request
   * @param \App\RequestHandler $handler Handler of request
   */
  public function get(string $pattern, \App\RequestHandler $handler, \App\MiddleWareSet $middleWareSet = null) {
    $this->route(\App\RequestMethods::get, $pattern, $handler, $middleWareSet);
  }

  /**
   * Add POST route to controller. This method is same thing as Controller->route(\App\RequestMethods::post, ..., ...);
   * @param string $pattern Pattern of URI to match
   * @param \App\MiddleWareSet Additional pre-handlers of request
   * @param \App\RequestHandler $handler Handler of request
   */
  public function post(string $pattern, \App\RequestHandler $handler, \App\MiddleWareSet $middleWareSet = null) {
    $this->route(\App\RequestMethods::post, $pattern, $handler, $middleWareSet);
  }

  /**
   * Add PUT route to controller. This method is same thing as Controller->route(\App\RequestMethods::put, ..., ...);
   * @param string $pattern Pattern of URI to match
   * @param \App\MiddleWareSet Additional pre-handlers of request
   * @param \App\RequestHandler $handler Handler of request
   */
  public function put(string $pattern, \App\RequestHandler $handler, \App\MiddleWareSet $middleWareSet = null) {
    $this->route(\App\RequestMethods::put, $pattern, $handler, $middleWareSet);
  }

  /**
   * Add DELETE route to controller. This method is same thing as Controller->route(\App\RequestMethods::delete, ..., ...);
   * @param string $pattern Pattern of URI to match
   * @param \App\MiddleWareSet Additional pre-handlers of request
   * @param \App\RequestHandler $handler Handler of request
   */
  public function delete(string $pattern, \App\RequestHandler $handler, \App\MiddleWareSet $middleWareSet = null) {
    $this->route(\App\RequestMethods::delete, $pattern, $handler, $middleWareSet);
  }

  /**
   * Checks that requested URI can be processed by one of controllers routes
   * @param $method
   */
  protected function canProcess($method) {
    return \App\RequestMethods::has($method) && in_array($method, array_keys($this->routes)); 
  }

  /**
   * Checks if controller can process request and executes request handler if matches
   * @param \App\Request $request Request
   * @param \App\Response $response Response
   * @return bool
   */
  public function __invoke(\App\Request &$request, \App\Response &$response): bool
  { 
    // ? If controller has no handlers for request method it will immediately return controll to application
    if(!$this->canProcess($request->method)) return false;
    // ? Checking for 
    foreach ($this->routes[$request->method] as $pattern => $handler) {
        if (preg_match($pattern, $request->rewritableUrl, $params)) {
            // Strip full match of URI
            array_shift($params);
            // ? Inject Request and Response to handler callback
            // Call request handler with URI params and request
            $handler($request, $response, $params);
            return true;
        }
    }
    // * No route matches found
    return false;
  }

}

?>
<?php 
  namespace App;

use AllowedMiddleWareTypes;

/**
 * Handler of request
 * @param callable $callback Function to process request
 */
class RequestHandler {
    protected $callback = null;
    protected ?\App\MiddleWareSet $middleWareSet = null;

    public function __construct(callable $callback)
    {
      $this->callback = $callback;
      $this->middleWareSet = new MiddleWareSet();
    }

    /**
     * Adds some additional middleware before request handler will execute
     * @param \App\MiddleWareSet|\App\MiddleWare $middleWare
     * 
     * @throws \App\NotAllowedType
     * @return void
     */
    public function use(object $middleWare): void {
      $this->middleWareSet->add($middleWare);
    }

    /**
     * Handler activation
     * @param \App\Request $request Request
     * @param \App\Response $response Response
     * @param array $params Array of additional params
     * @return void;
     */
    public function __invoke(\App\Request &$request, \App\Response &$response, array $params = null) {
      if(is_null($params)) $params = [];
      foreach ($this->middleWareSet->get() as $middleware) {
          $middleware($request, $response);
      }
      call_user_func_array($this->callback, array_merge([&$request, &$response], $params));
    }
  }
?>
<?php 
namespace App;

use AllowedMiddleWareTypes;
use App;

/**
 * MiddleWare list
 * @throws \App\NotAllowedType
 */
class MiddleWareSet {
  protected array $middleware = [];

  /**
   * @param array $middlewares Array of MiddleWares or MiddleWareSets to add
   * @throws \App\NotAllowedType
   * @return \App\MiddleWareSet
   */
  public function __construct(array $middlewares = null)
  {
    // ? Break constructor execution if no middlewares passed
    if(empty($middlewares)) return;
    // ? Add all passed middlewares to inherit list
    // ! Throw exception if some middleware have disallowed type
    foreach ($middlewares as $middleware) {
      $this->add($middleware);
    }
  }
  /**
   * Add middleware to inherit list
   * @param \App\MiddleWare|\App\MiddleWareSet $middleware
   * @return void
   */
  public function add(object $middleware): void {
    if(!AllowedMiddleWareTypes::allowed($middleware)) throw new NotAllowedType(get_class($middleware), "Not expected type of MiddleWare passed  into MiddleWare Set");
    $type = array_key_last(class_parents($middleware));
    if(is_null($type)) $type = get_class($middleware);
    switch($type) {
      case AllowedMiddleWareTypes::middleware:
        $this->middleware[] = $middleware;
        break;
      case AllowedMiddleWareTypes::middlewareset:
        $this->middleware = array_merge($this->middleware, $middleware->get());
        break;
      }

  }

  /**
   * Returns all iniherit MiddleWares
   * 
   * @return \App\MiddleWare[]
   */
  public function get() {
    return $this->middleware;
  }
}
?>
<?php
namespace App;

use App;

class Application
{

    use \App\Observer;

    protected static ?object $instance = null;

    /**
     * Creates instance of Application.
     * Next call of this method will return first created object
     *
     * @return \App\Application
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function create()
    {
        return self::instance();
    }

    protected \App\Request $request;
    protected \App\Response $response;

    protected \App\MiddleWareSet $middleware;
    /**
     * @var \App\Controller[]
     */
    protected array $controllers = [];

    protected function __construct()
    {
        $this->middleware = new \App\MiddleWareSet();
        $this->request = new \App\Request();
        $this->response = new \App\Response();
    }

    /**
     * Registers Controller or MiddleWare in application process
     *
     * @param \App\Controller|\App\MiddleWare $usage Controller||MiddleWare||MiddleWareSet to use
     *
     * @return void;
     *
     */
    function use (object $usage) : void {
        if (!AllowedUsageTypes::allowed($usage)) {
            throw new NotAllowedType(get_class($usage), "Not allowed type of usage passed into " . __METHOD__);
        }

        switch (array_key_last(class_parents($usage))) {
            case AllowedUsageTypes::middlewareType:
                $usage = $this->middleware->add($usage);
                break;
            case AllowedUsageTypes::controllerType:
                $usage->init();
                $this->controllers[] = $usage;
                break;
        }
    }
    /**
     * Runs application
     * @return void
     */
    public function run(): void
    {
        foreach ($this->middleware->get() as $middleware) {
            $middleware($this->request, $this->response);
        }
        foreach ($this->controllers as $controller) {
            if ($controller($this->request, $this->response)) {
                return;
            }
        }
        $this->response->headers()->setCode(\App\httpCode::notfound);
        $this->response->end('Not Found');
    }

    /**
     * Closes application and triggers *close* event
     */
    public static function close() {
        self::$instance->trigger('close');
        exit();
    }

}

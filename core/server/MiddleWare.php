<?php 
namespace App;

interface IMiddleWare {
  public function __invoke(\App\Request &$request, \App\Response &$response);
}
abstract class MiddleWare implements IMiddleWare {
  protected function inject(object $target, string $propertyName, &$value) {
    $target->{$propertyName} = $value;
  }
}
?>
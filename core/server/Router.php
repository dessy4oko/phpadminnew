<?php

namespace App;

class Router {

    protected array $routes = array();
    protected array $middleware;

    
    private function __construct() {}
    private function __clone() {}
    
    public function route($pattern, $callback) {
        $pattern = '/^' . $pattern . '$/';
        self::$routes[$pattern] = $callback;
    }

    public function clear() {
        self::$routes = [];
    }
    
    public function execute($url) {
        foreach (self::$routes as $pattern => $callback) {
            if (preg_match($pattern, $url, $params)) {
                array_shift($params);
                return call_user_func_array($callback, array_values($params));
            }
        }
        return false;
    }
}

?>
<?php 
  namespace App;
  // require_once dirname(__FILE__) . '/RequestQuery.trait.php';
  class Request {
    // ? URL params
    public string $url;
    public string $rewritableUrl;
    // IP
    public string $ip;
    // Request method
    public string $method;
    // Request body
    protected ?\App\HeadersDriver $headersDriver = null;


    public function __construct()
    {
      $this->url = $_SERVER['REQUEST_URI'];
      $this->rewritableUrl = $_SERVER['REQUEST_URI'];
      $this->ip = $_SERVER['REMOTE_ADDR'];
      $this->method = $_SERVER['REQUEST_METHOD'];
      $this->headersDriver = new \App\HeadersDriver();
    }

    /**
     * Creates object property by name
     */
    public function forceProperty(string $name, $value = null) {
      if(!preg_match("/^(?!\d|$)[\w\d]*$/u", $name)) throw new \Exception('Invalid property name passed to '.__METHOD__);
      if(!in_array($name, get_object_vars($this))) $this->{$name} = $value;
    }

    /**
     * Returns interface for $_GET params
     * @return 
     */
    public function query() {
      return new \App\RequestQuery();
    }

    public function headers() {
      return $this->headersDriver;
    }

    public function session() {
      // TODO: Return interface to manipulate session
    }

  }
?>
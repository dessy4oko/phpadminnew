<?php
namespace App;

use App\AllowedTypes;
use App\Enum;
use App\MiddleWare;
use App\NotAllowedType;
use App\Request;
use App\Response;

class BodyParser extends MiddleWare
{
    use \App\Errors;

    protected string $mode;
    protected array $body = [];
    protected array $files = [];
    public function __construct(string $mode = BodyParserMode::multipart)
    {
        $this->mode = $mode;
    }
    protected function flatFiles(array &$arr, array &$target) {
      foreach($arr as $index => $item) {
        if(is_array($item['name'])) {
          $parsed = [];
          $keys = array_keys($item);
          foreach($item['name'] as $index => $name) {
            $fileInfo = array_map(function($key) use ($item, $index) {
              return $item[$key][$index];
            }, $keys);
            $fileInfo = array_combine($keys, $fileInfo);
            $parsed[] = $fileInfo;
          }
          $this->flatFiles($parsed, $target);
          continue;
        }
        array_push($target, $item);
      }
    }
    protected function fileBodyParser(): void
    {
      $files = [];
      if (!empty($_FILES)) {
        $this->flatFiles($_FILES, $files);
      }
      foreach ($files as $index => $finfo) {
        $finfo['isuploaded'] = is_uploaded_file($finfo['tmp_name']);
        $files[$index] = (object)$finfo;
      }
      $this->files = $files;
    }
    protected function multipartBodyParser() : void {
      if(empty($_POST)) {
        return;
      }
      $params = [];
      foreach($_POST as $index => $value) {
        $params[$index] = $value;
      }

      $this->body = array_merge($this->body, $params);
    }

    protected function jsonBodyParser() : void {
      $params = [];
      try {
        $hInputStream = fopen('php://input', 'rb');
        if(!$hInputStream) {
          throw new \Exception('Can not open input stream');
        }
        $raw = '';
        while(!feof($hInputStream)) {
          $raw .= fread($hInputStream, 4096);
        }
        if(empty($raw)) return;
        $params = json_decode($raw, true);
        if(!$params) {
          throw new \Exception('Failed to decode input json file');
        }
      } catch (\Exception $e) {
        $this->provideError($e);
        $params = [];
      } finally {
        $this->body = array_merge($this->body, $params);
      }
    }

    public function __invoke(Request &$request, Response &$response)
    {
      if(empty($this->mode)) return;
      foreach(explode('|', $this->mode) as $mode) {
        if(!BodyParserMode::allowed($mode)) throw new NotAllowedType($mode, 'Body parser can`t process passed mode '.$mode);
        switch($mode) {
          case BodyParserMode::files:
            $this->fileBodyParser();
            $request->forceProperty('files', $this->files);
            break;
          case BodyParserMode::json:
            if(!preg_match("/^application\/json.*?$/u", $request->headers()->get('Content-Type'))) break;
            $this->jsonBodyParser();
            if($this->hasErrors()) {
              
              $errors = array_map(function($error) {
                return $error['message'];
              }, $this->getErrors());

              $response->json([
                'status' => 'error', 
                'message' => 'Errors occured while request body parser tried to reach json object : '
                              . implode(' ; ', $errors)
              ])->end();
            }
            $request->forceProperty('body', (object) $this->body);
            break;
          case BodyParserMode::multipart:
            $this->multipartBodyParser();
            $request->forceProperty('body', (object) $this->body);
            break;
        }
      }
    }
}

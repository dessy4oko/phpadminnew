<?php
namespace App;

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $arh = array();
        $rx_http = '/\AHTTP_/';
        foreach ($_SERVER as $key => $val) {
            if (preg_match($rx_http, $key)) {
                $arh_key = preg_replace($rx_http, '', $key);
                $arh_key = mb_strtolower($arh_key, \App\ConfigFabric::get()->encoding);
                $rx_matches = array();
                // do some nasty string manipulations to restore the original letter case
                // this should work in most cases
                $rx_matches = explode('_', $arh_key);
                if (count($rx_matches) > 0 and strlen($arh_key) > 2) {
                    foreach ($rx_matches as $ak_key => $ak_val) {
                        $rx_matches[$ak_key] = ucfirst($ak_val);
                    }

                    $arh_key = implode('-', $rx_matches);
                }
                $arh[$arh_key] = $val;
            }
        }
        return ($arh);
    }
}

class HeadersDriver
{

    protected array $headers = [];
    protected string $code = \App\httpCode::ok;

    public function __construct($raw = false)
    {
        !$raw && $this->createheaders();
    }

    protected function createheaders()
    {
        foreach (getallheaders() as $key => $value) {
            $this->headers[$key] = $value;
        }
        return;
    }

    protected function format(string $param)
    {
        return implode('-', array_map(function ($p) {
            return ucfirst($p);
        }, explode('-', $param)));
    }

    public function get(string $param)
    {
        $param = $this->format($param);
        if (!in_array($param, array_keys($this->headers))) {
            return null;
        }

        return $this->headers[$param];
    }

    /**
     * ? Attach header to response
     * @param string $param Param name
     * @param mixed $value Value of param
     * @return \App\HeadersDriver
     */
    public function set(string $param, $value) : \App\HeadersDriver
    {
        $param = $this->format($param);
        $this->headers[$param] = $value;
        return $this;
    }

    /**
     * Sets response code. Default is 200 OK. Returns result of code changing.
     * @param string $code one of \App\httpCode constants;
     * @return bool
     */
    public function setCode(string $code): \App\HeadersDriver {
      if(!\App\httpCode::has($code)) throw new InvalidHttpCode($code, "Invalid response code passed to ". __METHOD__);
      $this->code = $code;
      return $this;
    }

    /**
     * Sends headers
     */
    public function send():void {
        header($this->code, true);
        foreach ($this->headers as $param => $value) {
            header($param.':'.$value);
        }
    }

}

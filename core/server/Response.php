<?php

namespace App;

class Response
{
  protected string $response = '';
  
  protected ?\App\HeadersDriver $headersDriver = null;

  public function __construct()
  {
    $this->headersDriver = new \App\HeadersDriver(true);
  }

  /**
   * Get headers of response
   */
  public function headers() {
    return $this->headersDriver;
  }

  /**
   * Sets headers of response
   * @param array $headers
   * @return \App\Response
   */
  public function withHeaders(array $headers) : \App\Response {
    foreach ($headers as $key => $value) {
      $this->headersDriver->set($key, $value);
    }
    return $this;
  }

  /**
   * Response to client in json format
   * @param array $data Data to sent
   */
  public function json(array $data) : \App\Response {
    $json = json_encode($data);
    if(!$json) {
      $this->headers()->setCode(\App\httpCode::internalerror);
      $this->end('JSON encode error');
    }
    $this->headers()->set('Content-Type', 'application/json');
    $this->response = $json;
    return $this;
  }

  /**
   * Sets view render template as response to client.
   * 
   */
  public function view(string $templateName, array $data) : \App\Response {
    try {
      // * Throw exception if property not defined in config or folder list is empty
      if(gettype(\App\ConfigFabric::get()->views) === 'string' && \App\ConfigFabric::get()->views === '') throw new \Exception("You should specify views folder in application config. Set `views` property pointing to folder which contains templates.");
      // * Resolving views folder paths
      switch (gettype(\App\ConfigFabric::get()->views)) {
        case 'array':
          $views = array_map(function($folder) {
            return Path::resolve($folder);
          },\App\ConfigFabric::get()->views);
          break;
        default:
          $views = Path::resolve(\App\ConfigFabric::get()->views);
          break;
      }
  
      $loader = new \Twig\Loader\FilesystemLoader($views);
      $twig = new \Twig\Environment($loader, (array) \App\ConfigFabric::get()->twigOptions);

      $twig->addExtension(new \Twig\Extension\DebugExtension());

      $this->response = $twig->render($templateName, array_merge($data, ['config' => \App\ConfigFabric::get()]));

    } catch (\App\ConfigPropertyApsend $e) {
      // ? If views folder not specified then server will answer with 204 code immediately
      $this->headers()->setCode(httpCode::internalerror);
      $this->end($e->getMessage());
    } catch(\Exception $e) {
      // ? If views folder not specified then server will answer with 204 code immediately
      $this->headers()->setCode(httpCode::internalerror);
      $this->end($e->getMessage());
    }
    return $this;
  }

  /**
   * Sends response and terminates application runtime
   * @param string $response Plain response
   */
  public function end(string $response = ''): void {
    $this->headers()->send();
    echo !empty($response) ? $response : $this->response;
    \App\Application::close();
  }
}

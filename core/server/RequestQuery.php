<?php
namespace App;

interface httpQueryString
{
    public function has(string $name);
    public function all();
}
class RequestQuery implements httpQueryString
{
    /**
     * Check if $_GET has property with *$name*
     */
    public function has(string $name)
    {
        return isset($_GET[$name]);
    }

    /**
     * Returns all $_GET params
     */
    public function all()
    {
        return $_GET;
    }

    public function __get(string $name)
    {
        if (!isset($_GET[$name])) {
            return null;
        }

        return $_GET[$name];
    }
}

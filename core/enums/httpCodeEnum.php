<?php 
namespace App;
use App;
define('__PROTOCOL__', isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
class httpCode extends Enum {
  protected const protocol = __PROTOCOL__;
  public const ok = self::protocol . ' 200 OK';
  public const notfound = self::protocol . ' 404 Not Found';
  public const movedpermanently = self::protocol . ' 301 Moved Permanently';
  public const found = self::protocol . ' 302 Found';
  public const created = self::protocol . ' 201 Created';
  public const nocontent = self::protocol . ' 204 No Content';
  public const internalerror = self::protocol . ' 500 Internal Server Error';
  public const badrequest = self::protocol . ' 400 Bad Request';
}
?>
<?php 
namespace App;

class AllowedObserverType extends Enum {
    public const commonobserver = \App\Observer::class;
    
    use \App\UsesTrait;
}
?>
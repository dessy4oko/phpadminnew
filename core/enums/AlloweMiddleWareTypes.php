<?php

use App\Enum;
use App\MiddleWare;
use App\MiddleWareSet;

class AllowedMiddleWareTypes extends Enum {
  use \App\AllowedTypes;
  public const middlewareset = MiddleWareSet::class;
  public const middleware = MiddleWare::class;
}
?>
<?php 
  namespace App;

  class AllowedMimeTypes extends Enum {
    use \App\AllowedTypes;
    public const jpeg = "image/jpeg";
    public const png  = "image/png";
    public const svg  = "image/svg+xml";
    public const plain = "text/plain";
    public const json = "application/json";
  }
?>
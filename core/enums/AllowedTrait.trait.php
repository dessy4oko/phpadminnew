<?php 
    namespace App;

    trait UsesTrait {
        public static function allowed($object) {

            if(gettype($object) === 'string' && parent::has($object)) return true;
    
            $traits = class_uses($object);
            
            foreach ($traits as $type) {
                if(parent::has($type)) return true;
            }
    
            foreach (class_parents($object) as $parent) {
                if(self::allowed($parent)) return true;
            }
            return false;
        }
    }
?>
<?php
namespace App;

class AllowedUsageTypes extends Enum
{
    use \App\AllowedTypes;
    const middlewareType = MiddleWare::class;
    const middlewareSetType = MiddleWareSet::class;
    const controllerType = Controller::class;
}

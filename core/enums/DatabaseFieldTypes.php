<?php 
namespace App;

class DatabaseFieldTypes extends Enum {
  use AllowedTypes;
  public const tinyIntNumber = 'TINYINT';
  public const smallIntNumber = 'SMALLINT';
  public const intNumber = 'INT';
  public const floatNumber = 'FLOAT';
  public const dobuleNumber = 'DOUBLE';
  public const tinytext = 'TINYTEXT';
  public const text = 'TEXT';
  public const mediumtext = 'MEDIUMTEXT';
  public const largetext = 'LARGETEXT';
  public const varchar = 'VARCHAR';
  public const enum = 'ENUM';
  public const dateType = 'DATE';
  public const timeType = 'TIME';
  public const dateTimeType = 'DATETIME';
  public const timestamp = 'TIMESTAMP';
}

?>
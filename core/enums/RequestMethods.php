<?php 
namespace App;

class RequestMethods extends \App\Enum {
  const get = 'GET';
  const post = 'POST';
  const put = 'PUT';
  const delete = 'DELETE';
  const options = 'OPTIONS';
}
?>
<?php 
namespace App;

abstract class BodyParserMode extends Enum {
  use AllowedTypes;
  public const multipart = "multipart";
  public const files = "files";
  public const json = "json";
  /**
   * Combines parse modes
   */
  public static function join() : string {
    $modes = func_get_args();
    return implode('|', $modes);
  }
}

?>
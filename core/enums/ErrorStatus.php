<?php 
    namespace App;

    class ErrorStatus extends Enum {
        public const fatal = 'fatal';
        public const success = 'success';
        public const warnign = 'warning';
        use \App\AllowedTypes;
    }
?>
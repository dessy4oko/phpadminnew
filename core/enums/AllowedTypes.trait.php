<?php 
namespace App;
 trait AllowedTypes {
   /**
     * Checks if type is contained in allowed types enum
     *
     * @param $object Object to check
     *
     * @return bool
     */
    public static function allowed($object): bool
    {
        if (parent::has($object)) {
            return true;
        }

        foreach (static::fields() as $type) {
            if (is_a($object, $type)) {
                return true;
            }
        }
        return false;
    }
 }
?>
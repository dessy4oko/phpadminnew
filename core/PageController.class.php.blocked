<?php
    namespace App;
    class PageSourcesController
    {
        private $modules = [];
        private $pageSources = [];
        private $namedSourcesPtrs = [];
        private $errors = [];
        /**
         * Main class constructor. 
         * It gets all installed sources
         * 
         * @param array $page_source_data Unserialized page sources data from DB
         * 
         * @return PageSourcesController 
         */
        public function __construct($page_source_data = null)
        {
            if($page_source_data && gettype($page_source_data) === 'array') {
                $this->implementData($page_source_data);
            }
        }
        /**
         * Add sources wich contains in $sourceData
         * 
         * @param $source_data Array of sources and sources data
         * 
         * @return void
         */
        public function implementData(array $sourceData)
        {
            if(empty($sourceData)) return;

            $arraySourcesIds = [];
                
            foreach($sourceData as $data) {
                $arraySourcesIds[] = $data['id'];
            }

            $stringSourcesIds = implode(',', $arraySourcesIds);

            $sources = \dbquery(
                "SELECT
                    inf_id,
                    inf_title,
                    inf_folder,
                    source
                FROM
                    `".DB_INFUSIONS."`
                WHERE
                    source <> ''
                ORDER BY inf_title
            ");

            $sourcesInfo = [];
                
            if(dbrows($sources)) {
                while($module = dbarray($sources)) {
                    $sourcesInfo[$module['inf_id']] = $module;
                }
            }

            foreach($sourceData as $data) {
                if($data['statement'] && isset($sourcesInfo[$data['id']])) {
                    $module = $sourcesInfo[$data['id']];
                    $this->pageSources[] = $data['id'];
                    $this->namedSourcesPtrs[$module['source']] = $data['id'];
                    require INFUSIONS.$module['inf_folder'].'/sources/'.$module['source'].'.source.php';
                    $namespace = $module['source'].'\\Source';
                    $this->modules['' + $data['id']] = new $namespace($module['inf_id'],$module['inf_folder'], $module['inf_title'], $module['source']);
                    $this->modules[$data['id']]->setData($data['data']);
                }
            }
        }

        /**
         * Returns data array of source by id
         * 
         * @return array
         */
        public function getDataOf($name = null)
        {
            if(empty($name) || !isset($this->namedSourcesPtrs[$name])) {
                return;
            }
            $id = $this->namedSourcesPtrs[$name];
            return $this->modules[$id]->getData();
        }
        /**
         * Returns data array of source by name
         * 
         * @return array;
         */
        public function getDataOfName($name = null)
        {
            if(empty($name) || !isset($this->namedSourcesPtrs[$name])) {
                return;
            }
            $id = $this->namedSourcesPtrs[$name];
            return $this->modules[$id]->getData();
        }
        /**
         * Returns scripts array of source by id
         * 
         * @return array
         */
        public function getScriptsOf($id = null)
        {
            if(empty($id) || !is_numeric($id)) {
                return null;
            }
            return $this->modules[$id]->getScripts();
        }
        /**
         * Page sources markup rendering
         * 
         * @return array
         */
        public function renderSources($ARRAYED = false) {
            $markup = [];
            foreach ($this->pageSources as  $sourceId) {
                $markup [$this->modules[$sourceId]->getSourceName()] = $this->modules[$sourceId]->renderLayout();
            }
            if(!$ARRAYED) {
                foreach ($markup as $html) {
                    echo $html;
                }
            }
            return $markup;
        }
        /**
         * Provides linking to page sources scripts
         * 
         * @return null
         */
        public function linkSourcesScripts() {
            foreach ($this->pageSources as $sourceId) {
                $this->linkSourcesScriptsOf($sourceId);
            }
        }
        /**
         * Provides linking source with $sourceId scripts to page 
         * 
         * @param @sourceId page source identificator
         * 
         * @return null
         */
        public function linkSourcesScriptsOf($sourceId = null) {
            
            if(!isset($this->modules[$sourceId])) {
                return;
            }
            $scripts = $this->getScriptsOf($sourceId);
            if(!empty($scripts)) {
                foreach ($scripts as $script) {
                    \add_to_footer('<script src="'.$script['url'].'"></script>');
                }
            }
        }
        /**
         * Page sources markup display
         * 
         * @return null
         */
        public function displayPageLayout() {
            foreach ($this->pageSources as  $sourceId) {
                echo $this->modules[$sourceId]->renderLayout();
                $scripts = $this->getScriptsOf($sourceId);
                if(!empty($scripts)) {
                    foreach ($scripts as $script) {
                        \add_to_footer('<script src="'.$script['url'].'"></script>');
                    }
                }
            }
        }

        /**
         * Returns serialized data of sources input fields
         * 
         * @return string
         */
        public function processData ()
        {
            if(empty($_POST['sources'])) return '';
            $result = array();
            foreach ($_POST['sources'] as $id) {
                try {
                    // Check if SourceController contain source with recived id
                    if(!isset($this->modules[$id])) {
                        throw new \Exception('Source controller. Source module with identificator '.$id.' does not exist.');
                    }

                    if(isset($_POST['sources_statement']) && isset($_POST['sources_statement'][$id]) && is_numeric($_POST['sources_statement'][$id])) {
                        $statement = (int) $_POST['sources_statement'][$id];
                    } else {
                        $statement = 1;
                    }

                    // Get prepared data
                    $sourceData = $this->modules[$id]->prepareInput();
                    // Check if returned data is not empty and push it to result in not empty case
                    if(!empty($sourceData)) {
                        $result[] = array('id'=>$this->modules[$id]->getId(),'statement'=>$this->statement, 'data'=>$sourceData);
                    }

                } catch (\Exception $e) {
                    $this->errors[] = $e->getMessage();
                }

            }
            return serialize($result);
        }

    }
    
?>
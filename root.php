<?php

if(!function_exists('xdebug_var_dump')) {
  function xdebug_var_dump() {}
}

error_reporting(E_ALL ^ E_STRICT);
set_error_handler("setError");
function setError($error_level, $error_message, $error_file, $error_line, $error_context) {
  xdebug_var_dump($error_level, $error_message, $error_file, $error_line, $error_context);
}

# APPLICATION
require_once "./vendor/autoload.php";

use App\Path;
use App\Request;
use App\Response;

\App\ConfigFabric::load(dirname(__FILE__) . '/config.json');
Path::setEncoding(\App\ConfigFabric::get()->encoding);

require_once Path::resolve("~/api/main/Main.controller.php");
require_once Path::resolve("~/api/admin/Admin.controller.php");
require_once Path::resolve("~/api/resources/Resources.controller.php");
require_once Path::resolve("~/api/middleware/Logger.middleware.php");
require_once Path::resolve("~/api/middleware/AllowCORS.php");

use Main\MainController;
use Resources\ResourcesController;

$app = \App\Application::instance();

// $app->use(new \Admin\AdminController());

$app->use(new AllowCORS());

$app->use(new Logger(function(Request &$req, Response &$res){
  return $req->method.' '.$req->url."\n";
}));

// $app->use(new MainController());
$app->use(new ResourcesController());
$app->run();
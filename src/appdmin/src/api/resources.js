import axios from "./axios";

const uris = {
  upload: '/resources/',
  getfiles: '/resources/',
  deletefiles: '/resources/',
  makeDir: '/resources/'
};

export const upload = (data, onUploadProgress) => {
  return axios.post(uris.upload, data, {onUploadProgress});
};

export const getAll = (directory, onDownloadProgress) => {
  return axios.get(uris.getfiles, {directory}, {onDownloadProgress});
}

export const makeDir = (data) => {
  console.log('Make Dir data', data);
  return axios.put(uris.makeDir, data);
}

export const remove = (ids) => {
  return axios.delete(uris.deletefiles, {data: {
    resources: ids
  }});
}

import React, { Suspense } from "react";
import { Route } from "react-router-dom";
import { Carcaas, Loader, Drophandler } from "./components";
import { Provider } from "react-redux";
// Api import
import store from "./redux/store";

const Dashboard = React.lazy(() => import("./pages/Dashboard"));
const Users = React.lazy(() => import("./pages/Users"));
const Resources = React.lazy(() => import("./pages/Resources"));
const System = React.lazy(() => import("./pages/System"));
const Plugins = React.lazy(() => import("./pages/Plugins"));
const Profile = React.lazy(() => import("./pages/Profile"));



function App() {
  return (
    <div className="touchdot-app">
      <Provider store={store}>
        <Drophandler handler={() => {}}>
          <Carcaas>
            <Suspense fallback={<Loader />}>
              <Route exact path="/">
                <Dashboard />
              </Route>
              <Route exact path="/users">
                <Users />
              </Route>
              <Route exact path="/settings">
                <System />
              </Route>
              <Route exact path="/resources">
                <Resources />
              </Route>
              <Route exact path="/plugins">
                <Plugins />
              </Route>
              <Route exact path="/profile">
                <Profile />
              </Route>
            </Suspense>
          </Carcaas>
        </Drophandler>
      </Provider>
    </div>
  );
}

export default App;

export const uploadMultiplefilesHandler = (files, upload, notify) => {
  for (let i = 0; i < files.length; i++) {
    let data = new FormData();
    const file = files[i];
    data.append("file", file);
    upload(data)
      .then((response) => {
        console.log(response);
        if (response.data.status === "error") {
          notify({
            status: "error",
            title: "Файл не был загружен",
            body: [
              `Не удалось загрузить ${file.name}.`,
              <br />,
              response.data.message,
            ],
            delay: 3500,
          });
        }
      })
      .catch((error) => {
        if (typeof error.response === typeof undefined) {
          notify({
            status: "error",
            delay: 3000,
            title: error.message,
            body:
              "Не удалось получить данные от сервера. Проверьте подключение с интернетом. Если вы уверены, что это не является причиной данной ошибки, обратитесь в техническую поддержку приложения.",
          });
        } else {
          notify({
            status: "error",
            title: "Файл не был загружен",
            body: [
              `Не удалось загрузить ${file.name}.`,
              <br />,
              error.response.data.message,
            ],
            delay: 3500,
          });
        }
      });
    data = null;
  }
};
export const createFolderHandler = function(folderName, parentDirectory, createDirectory, notify) {
  return createDirectory(folderName, parentDirectory)
    .then((response) => {
      if (response.data.status === "error") {
        notify({
          status: "error",
          title: "Папка не была создана",
          body: [
            `Не удалось создать папку с именем ${folderName}.`,
            <br />,
            response.data.message,
          ],
          delay: 3500,
        });
      }
      return response;
    })
    .catch((error) => {
      if (typeof error.response === typeof undefined) {
        notify({
          status: "error",
          delay: 3000,
          title: error.message,
          body:
            "Не удалось получить данные от сервера. Проверьте подключение с интернетом. Если вы уверены, что это не является причиной данной ошибки, обратитесь в техническую поддержку приложения.",
        });
      } else {
        notify({
          status: "error",
          title: "Папка не была создана",
          body: [
            `Не удалось создать папку с именем ${folderName}.`,
            <br />,
            error.response.data.message,
          ],
          delay: 3500,
        });
      }
    });
}

import {combineReducers} from 'redux'
import resources from './resources';
import notifications from './notifications'

export default combineReducers({
  resources,
  notifications
});
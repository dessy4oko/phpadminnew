const initialState = {
  allshown: false,
  notifications: []
}

const reducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case '@notifications:notify':
      return {
        ...state,
        notifications: state.notifications.concat([payload])
      };
    case '@notifications:remove':
      return {
        ...state,
        notifications: state.notifications.filter(notification => (notification.uid !== payload))
      };
    case '@notifications:removeAll':
      return {
        ...state,
        notifications: [],
      }
    case '@notifications:showAll':
      return {
        ...state,
        allshown: payload,
      }
    default:
      return state;
  }
}

export default reducer;
const initialState = {
  files: [],
  uploading: [],
  currentDirectory: '/'
}

const reducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case '@resources:setFiles':
      return {
        ...state,
        files: payload
      };
    case '@resources:addFile':
      console.log(payload);
      return {
        ...state,
        files: state.files.concat(payload)
      };
    case '@resources:setUploadingFiles':
      return {
        ...state,
        uploading: payload
      }
    case '@resources:addFiles':
      return {
        ...state,
        files: [...state.files, ...payload]
      }
    default:
      return state;
  }
}

export default reducer;
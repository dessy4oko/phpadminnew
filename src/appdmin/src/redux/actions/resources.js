import {resources as api} from '../../api';

const actions = {
  upload: (fileData, onUploadProgress = null) => dispatch => {
    return api.upload(fileData, onUploadProgress).then(response => {
      (response.data.status !== 'error') && dispatch(actions.addFile(response.data));
      return response;
    })
  },
  addFile: (file) => ({
    type: '@resources:addFile',
    payload: file
  }),
  setFiles: (files) => ({
    type: '@resources:setFiles',
    payload: files

  }),
  addFiles: (files) => ({
    type: '@resources:addFiles',
    payload: files
  }),
  setUploading: files => ({
    type: '@resources:setUploadingFiles',
    payload: files
  }),
  getFiles: (directory = '') => dispatch => {
    return api.getAll(directory).then(response => {
      (response.data.status !== 'error') && dispatch(actions.addFiles(response.data));
      return response;
    });
  },
  removeFiles: (ids) => dispatch => {
    return api.remove(ids).then(response => {
      (response.data.status !== 'error') && dispatch({
        type: '@resources:removeFile',
        payload: ids
      });
      return response;
    });
  },
  createDirectory: (dirname, parent) => dispatch => {
    return api.makeDir({dirname, parent}).then(response => {
      (response.data.status !== 'error') && dispatch(actions.addFile(response.data.folder));
      return response;
    })
  }
}

export default actions;
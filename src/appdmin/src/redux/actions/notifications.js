const actions = {
  notify: ({ title, body, status, delay }) => (
    {
      type: "@notifications:notify",
      payload: {
        uid: Date.now() + Math.floor(Math.random() * 100),
        title: title || '',
        body: body || '',
        status: status || 'main',
        delay: delay || 0
      }
    }
  ),
  remove: (identificator) => ({
    type: "@notifications:remove",
    payload: identificator,
  }),
  removeAll: () => ({
    type: '@notifications:removeAll'
  }),
  networkError: (error) => (dispatch) => {
    dispatch(
      actions.notify({
        status: 'error',
        delay: 3000,
        title: error.message,
        body: "Не удалось получить данные от сервера. Проверьте подключение с интернетом. Если вы уверены, что это не является причиной данной ошибки, обратитесь в техническую поддержку приложения."
      })
    );
  },
  showAll: (show) => ({
    type: '@notifications:showAll',
    payload: show
  })
};

window.notifications = actions;

export default actions;

import React from 'react'
import { Button } from '../../components';
import { Button as AntButton } from 'antd';
import { FullscreenOutlined } from '@ant-design/icons';

const Dashboard = () => {
    return (
        <>
            <h1>Hello, dashboard!</h1>
            <Button htmlType="submit" type="bond" loading outlined>Common submit</Button>
            <Button type="alter">Alter</Button>
            <Button type="primary">Primary</Button>
            <Button htmlType="button" type="warning" loading icon={<FullscreenOutlined />}></Button>
            <Button htmlType="reset" type="main">Main reset</Button>
            <Button htmlType="submit" type="soft" outlined icon={<FullscreenOutlined />}>Soft icon</Button>
            <Button htmlType="submit" type="primary" outlined loading>Outlined</Button>
            <AntButton type="primary"loading></AntButton>
        </>
    )
}

export default Dashboard;
import React, { useCallback, useEffect, useRef, useState } from "react";
import { Row, Col, Empty, Popconfirm, Input } from "antd";
import { connect } from "react-redux";
import { CloudUploadOutlined, FolderOpenOutlined, PlusOutlined } from "@ant-design/icons";
// Components
import {
  Button,
  ButtonGroup,
  FileItem,
  UploadButton,
  Tree,
  Modal
} from "../../components";
// Actions
import { resourcesActions, notificationsActions } from "../../redux/actions";
// Handlers
import { uploadMultiplefilesHandler, createFolderHandler } from "../../handlers/resources";

// TODO: 1. Сделать прогресс бар для файлов
// TODO: 2. Сделать превью файлов по двойному клику
// TODO: 3. Инструменты для работы с файлами

const Resources = ({
  files,
  currentDirectory,
  upload,
  getFiles,
  removeFiles,
  createDirectory,
  notify,
  networkError,
}) => {
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [folderModalVisible, setFolderModalVisible] = useState(false);
  const inputRef = useRef(null);

  const clearFilesSelection = useCallback(() => {
    console.log("some");
    selectedFiles.length && setSelectedFiles([]);
  }, [selectedFiles, setSelectedFiles]);

  useEffect(() => {
    window.addEventListener("click", clearFilesSelection);
    return () => {
      window.removeEventListener("click", clearFilesSelection);
    };
  }, [clearFilesSelection]);

  useEffect(() => {
    getFiles(currentDirectory);
  }, [currentDirectory]);

  const deleteFiles = (e) => {
    e.preventDefault();
    e.stopPropagation();
    console.log(selectedFiles);
    if (!selectedFiles.length) return;
    removeFiles(selectedFiles)
      .then((response) => {
        if (response.data.status === "error") {
          notify({
            status: "error",
            title: "Не удалось удалить указанные файлы",
            body: [response.data.message],
            delay: 3500,
          });
        } else {
          notify({
            status: "success",
            title: response.data.message,
            body: response.data.body,
            delay: 3500,
          });
        }
        setSelectedFiles([]);
      })
      .catch((error) => {
        if (typeof error.response === typeof undefined) {
          networkError(error);
        } else {
          notify({
            status: "error",
            title: "Файлы не были удалены",
            body: error.response.data.message,
            delay: 3500,
          });
        }
      });
  };

  const handleClick = (e, id = null) => {
    e.preventDefault();
    e.stopPropagation();

    setSelectedFiles((current) => {
      if (!id) return [];
      if (e.shiftKey) {
        // Clear selection
        if (window.getSelection) {
          if (window.getSelection().empty) {
            // Chrome
            window.getSelection().empty();
          } else if (window.getSelection().removeAllRanges) {
            // Firefox
            window.getSelection().removeAllRanges();
          }
        } else if (document.selection) {
          // IE?
          document.selection.empty();
        }

        console.log(id);
      }
      if (e.ctrlKey) {
        if (!current.includes(id)) {
          current = current.concat([id]);
        } else {
          current = current.filter((uid) => id !== uid);
        }
        return current;
      }
      if (!current.includes(id) || current.length > 1) {
        current = [id];
      } else {
        current = [];
      }
      return current;
    });
  };

  const handleCreateFolder = (folderName) => {
    createFolderHandler(folderName, '/', createDirectory, notify)
      .then(response => {
        if(response.data.status === 'success') {
          setFolderModalVisible(false); 
        }
      })
  }
  return (
    <>
      <h1 className="touchdot-page-header">Ресурсы</h1>
      <Row>
        <Col span={6}>
          <Tree
            items={[{ name: "Основная директория", parent: null }].concat(
              files.filter(function (file) {
                return file.type === "folder";
              })
            )}
          />
        </Col>
        <Col span={18}>
          <div className="touchdot-page-tools">
            <ButtonGroup>
              <Button
                dashed
                type="soft"
                htmlType="button"
                icon={<PlusOutlined />}
                onClick={() => setFolderModalVisible(true)}
              >
                Создать папку
              </Button>
              <UploadButton
                multiple
                dashed
                onChange={(files) => uploadMultiplefilesHandler(files, upload, notify)}
                type="primary"
                icon={<CloudUploadOutlined />}
              >
                Загрузить
              </UploadButton>
              {!!selectedFiles.length && (
                <Popconfirm
                  title="Вы действительно хотите удалить эти файлы?"
                  onConfirm={deleteFiles}
                  okText="Удалить"
                  cancelText="Нет"
                >
                  <Button
                    onClick={(e) => {e.stopPropagation();}}
                    type="warning"
                    htmlType="button"
                  >
                    Удалить
                  </Button>
                </Popconfirm>
              )}
            </ButtonGroup>
          </div>
          {files.length ? (
            <>
              {files.map((file) => {
                file.checked = selectedFiles.includes(file.uid);
                return (
                  <FileItem type="checkbox" onClick={handleClick} key={file.uid} {...file} />
                );
              })}
            </>
          ) : (
            <>
              <Empty description="Папка пуста"></Empty>
            </>
          )}
        </Col>
      </Row>
      <Modal
        title="Введите название папки"
        type='success'
        visible={folderModalVisible}
        onClose={() => setFolderModalVisible(false)}
        footer={
          <ButtonGroup flow="end">
            <Button key="cancel" onClick={() => setFolderModalVisible(false)} outlined type="soft" htmlType="button">Отменить</Button>
            <Button key="create" onClick={() => { inputRef.current.input && handleCreateFolder(inputRef.current.input.value) }} icon={<PlusOutlined/>} outlined type="success" htmlType="button">Создать</Button>
          </ButtonGroup>
        }
      >
        <Input
          ref={inputRef}
          addonBefore={'/resources' + currentDirectory}
          onPressEnter={() => {inputRef.current.input && handleCreateFolder(inputRef.current.input.value)}}
          prefix={<FolderOpenOutlined/>}
        />
      </Modal>
    </>
  );
};

export default connect(({ resources }) => resources, {
  ...resourcesActions,
  ...notificationsActions,
})(Resources);

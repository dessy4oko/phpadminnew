export {default as Dashboard} from './Dashboard';
export {default as Users} from './Users';
export {default as Resources} from './Resources';
export {default as System} from './System';
export {default as Plugins} from './Plugins';
export {default as Profile} from './Profile';
class Humanizer {
    constructor() {
        const degreeChecker = function ($num) {
            if ($num < 1000) {
                return 0;
            }
            return 1 + degreeChecker($num / 1000);
        };

        this.degreeChecker = degreeChecker;
        this.decimalsCount = function (num) {
            return num !== Math.floor(num) ? num.toString().split(".")[1].length : 0;
        };
    }

    bytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    
    }

    price(price) {
        if (typeof price !== "number") {
            return price;
        }

        price = Number(price);

        let degree = this.degreeChecker(price);
        let res = 0;
        while (
            degree !== 0 &&
            (res = parseFloat(price / 1000 ** degree)) &&
            this.decimalsCount(res) > 3
        ) {
            --degree;
        }
        switch (degree) {
            case 1:
                return res + " тыс.";
            case 2:
                return res + " млн.";
            case 3:
                return res + " млрд.";
            default:
                return price;
        }
    }

}

export default Humanizer;
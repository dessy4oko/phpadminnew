import React from 'react';
import classNames from 'classnames'

const ButtonGroup = (props) => (
  <div className={classNames('touch-btn-group', {'touch-btn-group-end': (props.flow === 'end')})}>
    {props.children}
  </div>
);

export default ButtonGroup;
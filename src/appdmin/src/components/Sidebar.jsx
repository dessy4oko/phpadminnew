// Libs
import React from 'react'
import classNames from 'classnames';
import { NavLink } from 'react-router-dom'
import { Button, Tooltip, Divider, Layout } from 'antd';
import { LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
// Components
import { Logotype, Menu } from './index';
const { Sider } = Layout;

const Sidebar = ({ width, ...props }) => {
    return (
        <Sider width={width} className={classNames(props.className, 'touchdot-app-sidebar')}>
            <Layout className="touchdot-app-sidebar-content">
                <NavLink to="/">
                    <Logotype className="touchdot-app-logotype" src={process.env.PUBLIC_URL + 'logotype.svg'} />
                </NavLink>
                <Divider className="touchdot-app-logotype-divider" />
                <Menu className="touchdot-menu" />
                <Divider />
                <div className="touchdot-app-tools">
                    <NavLink to="/profile">
                        <Tooltip title="Профиль">
                            <Button shape="circle" type="dashed" icon={<UserOutlined />} />
                        </Tooltip>
                    </NavLink>
                    <Divider type="vertical" />
                    <NavLink to="/settings">
                        <Tooltip title="Настройки">
                            <Button shape="circle" type="dashed" icon={<SettingOutlined />} />
                        </Tooltip>
                    </NavLink>
                    <Divider type="vertical" />
                    <Tooltip title="Выйти">
                        <Button shape="circle" type="dashed" icon={<LogoutOutlined />} />
                    </Tooltip>
                </div>
            </Layout>
        </Sider>
    );
}

export default Sidebar;
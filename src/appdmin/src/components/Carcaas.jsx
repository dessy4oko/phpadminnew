// Libraries
import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import { Layout } from 'antd';
// Components
import { Sidebar, Header, Notifications } from './index';

const { Content } = Layout;

const Carcaas = (props) => {
    const siderWidth = 278;
    return (
        <Layout className="touchdot-app-main__layout">
            <Router>
                <Sidebar width={siderWidth} />
                <Content className="touchdot-app-content" style={{marginLeft: siderWidth}}>
                    <Header />
                    <Switch>
                        {props.children}
                    </Switch>
                </Content>
            </Router>
            <Notifications />
        </Layout>
    )
};

export default Carcaas;
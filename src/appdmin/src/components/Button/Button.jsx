import React from "react";
// import {Button as AntButton} from 'antd'
import propTypes from 'prop-types'
import classNames from 'classnames';

const Button = ({ htmlType, onClick, disabled, ...props }) => {
    const classes = classNames(
        'touch-btn',
        {
            'touch-btn-primary': !props.type,
            [`touch-btn-${props.size}`]: props.size,
            [`touch-btn-${props.type}`]: props.type,
            'touch-btn-outlined': props.outlined || props.dashed,
            'touch-btn-loading': props.loading,
            'touch-btn-icon-only': (!props.children || props.children.length === 0),
            'touch-btn-rounded' : props.rounded,
            'touch-btn-dashed': props.dashed,
            'touch-btn-transparent': props.transparent
        },
    );

    return (
        <button type={htmlType} className={classes} onClick={onClick}>
            {props.icon && !props.loading && (<span className="touch-btn-icon">{props.icon}</span>)}
            {props.loading && (<span className="touch-btn-icon"><span className="touch-btn-spiner"></span></span>)}
            {props.children && (<span>{props.children}</span>)}
        </button>
    )
}

Button.propTypes = {
    type: propTypes.oneOf(['primary', 'main', 'alter', 'transparent', 'soft', 'bond', 'warning', 'success']),
    htmlType: propTypes.oneOf(['button', 'submit', 'reset']),
    onClick: propTypes.func
}

export default Button;
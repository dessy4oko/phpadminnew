import React from 'react'

const Loader = () => {
    return (
        <div className="touchdot-loader">
            <img src={process.env.PUBLIC_URL + 'arrow.svg'} alt="Fast Arrow" className="touchdot-loader-arrow" />
            <div className="touchdot-loader-title">Загрузка...</div>
            <div className="body">
                <span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </div>
            <div className="longfazers">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    );
}

export default Loader;
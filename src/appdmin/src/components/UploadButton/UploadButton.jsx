// Libs
import React from 'react'
// Components
import { Button } from '../index'

const UploadButton = ({onChange, multiple, ...props}) => {
    const handleClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        const input = document.createElement('input')
        input.type = 'file';
        input.multiple = !!multiple;
        input.onchange = (e => {
            let files = input.files;
            onChange && onChange(files);
        });
        input.click();
    }

    return (
        <Button onClick={e => handleClick(e)} type="transparent" {...props} htmlType="button"></Button>
    )
}

export default UploadButton;
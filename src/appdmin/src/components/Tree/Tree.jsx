import React from 'react'
import TreeItem from '../TreeItem'

const Tree = ({items}) => {
  items = [
    {
      uid: 1,
      name: 'Основная директория',
      childs: [
        {
          uid: 2,
          name: 'some',
          childs: []
        },
        {
          uid: 3,
          name: 'onemore',
          childs: []
        }
      ]
    }
  ]
  return (
    items.map(item => {
      return <TreeItem key={item.uid} {...item} />
    })
  )
}

export default Tree;
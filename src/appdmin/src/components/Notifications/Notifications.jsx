import React from "react";
import classNames from "classnames";
import { connect } from "react-redux";
import { CheckCircleOutlined, ClearOutlined, InfoCircleOutlined, WarningOutlined } from "@ant-design/icons";
import { Drawer, Empty } from 'antd'

import { Notification, Button } from "../";
import { notificationsActions } from "../../redux/actions";
const Notifications = ({ notifications,allshown, remove, removeAll, showAll }) => {

  const closeHandler = (uid) => {
    remove(uid);
  };

  const drawerCloseHandler = () => {
    showAll(false);
  }

  const clearAllTrigger = () => {
    removeAll();
  }

  return (
    <>
      <Drawer
        width={580}
        onClose={drawerCloseHandler}
        closable={false}
        visible={allshown}
        title="Уведомления"
        footer={
          <Button type="warning" dashed onClick={clearAllTrigger} icon={<ClearOutlined />}>Очистить</Button>
        }
      >
      {
        // eslint-disable-next-line array-callback-return
        notifications.length ? notifications.map((notification) => {
          let icon = null;
          let type = 'soft';
          switch (notification.status) {
            case 'error':
              icon = <WarningOutlined />;
              type = 'warning'
              break;
            case 'primary':
              icon = <InfoCircleOutlined />;
              type = 'primary'
              break;
            case 'success':
              icon = <CheckCircleOutlined />
              type = 'success'
              break;
            default:
              icon = null
              type = null
          }
          return (
            <Notification
              key={notification.uid}
              onClose={() => closeHandler(notification.uid)}
              title={notification.title}
              icon={icon}
              type={type}
            >
              {notification.body}
            </Notification>
          );
        }) : <Empty description="Нет новых уведомлений" />
      }
      </Drawer>
      <div
        className={classNames("choko-notifications-box", {
          "choko-notifications-shown": notifications.length,
        })}
      >
        {
          // eslint-disable-next-line array-callback-return
          notifications.map((notification) => {
            if (notification.uid + notification.delay < Date.now()) return null;
            let icon = null;
            let type = 'soft';
            switch (notification.status) {
              case 'error':
                icon = <WarningOutlined />;
                type = 'warning'
                break;
              case 'primary':
                icon = <InfoCircleOutlined />;
                type = 'primary'
                break;
              case 'success':
                icon = <CheckCircleOutlined />
                type = 'success'
                break;
              default:
                icon = null
                type = null
            }
            return (
              <Notification
                key={notification.uid}
                onClose={() => closeHandler(notification.uid)}
                title={notification.title}
                icon={icon}
                type={type}
                delay={notification.delay || null}
              >
                {notification.body}
              </Notification>
            );
          })
        }
      </div>
    </>
  );
};

export default connect(({ notifications}) => notifications, notificationsActions)(Notifications);

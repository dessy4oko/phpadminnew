import React from "react";
import classNames from "classnames";

const Badge = (props) => {
  return (
    <span className="touch-badge">
      {props.children}
      {props.active && (
        <sup
          className={classNames("touch-badge-dot", {
            "touch-badge-dot-primary": !props.type,
            [`touch-badge-dot-${props.type}`]: props.type,
          })}
        >
          {props.count ? props.count : ""}
        </sup>
      )}
    </span>
  );
};

export default Badge;

import React from 'react'
import classNames from 'classnames';

const Logo = ({src, ...props}) => (
    <div className={classNames(props.className)}>
        <img src={src} alt={props.alt ? props.alt : 'TouchDot dashboard logotype'}/>
    </div>
);

export default Logo;
import React from "react";
import { connect } from "react-redux";
import { NotificationOutlined, PlusOutlined } from "@ant-design/icons";
// Components
import { Badge, Button } from "./";
import { notificationsActions } from '../redux/actions';

const Header = ({notifications, allshown, showAll}) => {

  const notificationsButtonClick = () => {
    showAll(!allshown);
  }

  return (
    <header className="touchdot-app-header">
      <div className="touchdot-header-searchbar">
        <input type="text" name="query" className="touchdot-control" />
      </div>
      <div className="touchdot-header-tools">
        <Button
            type="primary"
            htmlType="button"
            transparent
            size="large"
            icon={<PlusOutlined />}
        />
        <Button
          onClick={notificationsButtonClick}
          type="primary"
          htmlType="button"
          transparent
          size="large"
          icon={<Badge active={!!notifications.length} type="warning"><NotificationOutlined /></Badge>}
        ></Button>
      </div>
    </header>
  );
};

export default connect(({notifications})=> notifications, notificationsActions)(Header);

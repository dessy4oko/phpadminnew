import {
  CheckOutlined,
  FileImageOutlined,
  FileOutlined,
  FileTextOutlined,
  FolderOutlined,
} from "@ant-design/icons";
import React from "react";
import classNames from "classnames";
import propTypes from "prop-types";

import { Humanizer } from "../../helpers";

const FileItem = (props) => {
  let defaultProps = {
    type: "checkbox",
    mimetype: "file",
  };
  props = Object.assign({}, defaultProps, props);
  const formatter = new Humanizer();
  let icon = null;
  switch (props.mimetype) {
    case "image/png":
    case "image/jpeg":
    case "image/gif":
      icon = <FileImageOutlined />;
      break;
    case "image/svg+xml":
      case "text/plain":
      icon = <FileTextOutlined />;
      break;
    case "folder":
        icon = <FolderOutlined />;
        break;
    default:
      icon = <FileOutlined />;
  }
  return (
    <div
      className={classNames("touchdot-file", { checked: props.checked, 'touchdot-folder' : (props.mimetype === 'folder') })}
      onClick={(e) => {
        props.onClick && props.onClick(e, props.uid);
      }}
    >
      {props.type === "checkbox" && (
        <div
          className="touchdot-file-checkbox"
          onClick={(e) => {
            e.ctrlKey = true;
            props.onClick && props.onClick(e, props.uid);
          }}
        >
          <CheckOutlined />
        </div>
      )}
      <div className="touchdot-file-icon">
        { icon }
      </div>
      <div className="touchdot-file-options">
        <div className="touchdot-file-name">{props.name}</div>
        <div className="touchdot-file-size">{formatter.bytes(props.size)}</div>
      </div>
    </div>
  );
};

FileItem.propTypes = {
  name: propTypes.string.isRequired,
  size: propTypes.number.isRequired,
  type: propTypes.string,
  onClick: propTypes.func,
};

export default FileItem;

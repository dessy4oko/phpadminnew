import React from 'react'
import { Menu as AntMenu } from 'antd';
import {
    AppstoreAddOutlined,
    UserOutlined,
    FolderOpenOutlined,
    LayoutOutlined
} from '@ant-design/icons';
import {
    withRouter,
    NavLink
} from 'react-router-dom';
import classNames from 'classnames';

const paths = [
    {
        url: '/',
        icon: <LayoutOutlined className="touchdot-menu-icon" />,
        caption: 'Центр управления'
    },
    {
        url: '/users',
        icon: <UserOutlined className="touchdot-menu-icon" />,
        caption: "Пользователи"
    },
    {
        url: '/resources',
        icon: <FolderOpenOutlined className="touchdot-menu-icon" />,
        caption: "Ресурсы"
    },
    {
        url: '/plugins',
        icon: <AppstoreAddOutlined className="touchdot-menu-icon" />,
        caption: "Плагины"
    }
]

const Menu = (props) => {
    const { location } = props;
    return (
        <AntMenu
            className={classNames(props.className)}
            selectedKeys={[location.pathname]}>
            {
                paths.map((path) => {
                    return (
                        <AntMenu.Item
                            key={path.url}
                            className={classNames('touchdot-menu-item',{ 'touchdot-menu-item-selected': location.pathname === path.url }, path.classNames)}
                            icon={path.icon}>
                            <NavLink to={path.url}>{path.caption}</NavLink>
                        </AntMenu.Item>
                    )
                })
            }
        </AntMenu>
    )
}

export default withRouter(Menu);
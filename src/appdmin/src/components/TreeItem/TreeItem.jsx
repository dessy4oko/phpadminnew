import React from 'react'

const TreeItem = ({name, childs}) => {
  return(
    <div className="touch-tree__item">
      <span className="touch-tree__item-name">{name}</span>
      {childs && childs.map(item => {
        
      })}
    </div>
  )
}

export default TreeItem;
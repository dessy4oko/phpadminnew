import React, { useCallback, useEffect, useMemo } from "react";
import { CloseOutlined } from "@ant-design/icons";
import classNames from "classnames";
import { Button } from "../index";
import reactDom from "react-dom";

const Modal = ({ children, visible, onClose, title, footer }) => {
  let modalContainer = useMemo(() => {
    return document.createElement('div');
  });
  useEffect(() => {
    document.body.appendChild(modalContainer);
    return () => {
      document.body.removeChild(modalContainer)
    }
  }, [modalContainer])

  return (visible && reactDom.createPortal(
    <div className="touch__modal-root">
      <div className="touch__modal-backdrop" role="dialog" tabIndex="-1">
        <div className="touch__modal-wrapper">
          <div className="touch__modal-content">
            <header
              className={classNames("touch__modal-header")}
            >
              <h3>{title}</h3>
              <div className="touch__modal-close">
                <Button
                  icon={<CloseOutlined />}
                  outlined
                  type="soft"
                  size="small"
                  htmlType="button"
                  onClick={onClose}
                ></Button>
              </div>
            </header>
            <section className="touch__modal-main">{children}</section>
            {footer && (
              <footer className="touch__modal-footer">{footer}</footer>
            )}
          </div>
        </div>
      </div>
    </div>, modalContainer)
  );
};

export default Modal;

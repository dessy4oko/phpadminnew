import React, { useEffect } from 'react'
import classNames from 'classnames'

const Notification = ({ title, type, icon, delay, ...props }) => {
    
    const notifRef = React.createRef();
    let handler = null;

    const clickHandler = () => {
        handler && clearTimeout(handler);
        hideNotification();
        setTimeout(() => {
            props.onClose && props.onClose();
        }, 500);
    }

    const hideNotification = () => {
        const notification = notifRef.current;
        if(!notification) return;
        notification.style.height = '0px';
        notification.style.opacity = 0;
    }

    useEffect(() => {
        const notification = notifRef.current;
        const height = notification.querySelector('div').clientHeight;
        console.log(height, notification.querySelector('div'));
        notification.style.height = height + 'px';
        notification.style.opacity = 1;
        delay && (handler = setTimeout(() => {
            hideNotification();
        }, delay));
    }, [notifRef]);

    return (
        <div onClick={(e) => clickHandler(e)} ref={notifRef} className={classNames(
            'touch-notification',
            {
                [`touch-notification-${type}`] : type,
                'touch-notification__iconed' : icon
            }
        )}>
            <div className="touch-notification__inner">
                {icon && (<div className="touch-notification__icon">{icon}</div>)}
                <div className="touch-notification__text">
                    {title && (<div className="touch-notification__title">{title}</div>)}
                    {!!props.children.length && (<div className="touch-notification__content">{props.children}</div>)}
                </div>
            </div>
        </div>
    );
}

export default Notification;
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import { CloudUploadOutlined } from "@ant-design/icons";
// Actions
import { resourcesActions, notificationsActions } from "../../redux/actions";

// TODO: 1. Сделать в правом нижнем углу компонент, отражающий загрузку файлов

const Drophandler = ({ upload, networkError, notify, ...props }) => {
  const [state, setState] = useState({
    blocked: false,
    draggin: false,
    length: 0,
  });

  const [enterCount, setCount] = useState(0);

  const dropRef = React.createRef();

  const handleDragEnter = (e) => {
    if (state.blocked) return;
    e.preventDefault();
    e.stopPropagation();
    setCount((count) => count + 1);
    e.dataTransfer.items &&
      e.dataTransfer.items.length &&
      setState({
        blocked: false,
        draggin: true,
        length: e.dataTransfer.items.length,
      });
  };

  const handleDragLeave = (e) => {
    if (state.blocked) return;
    e.preventDefault();
    e.stopPropagation();
    setCount((count) => count - 1);
    if (enterCount > 0) return;
    setState({ blocked: false, draggin: false, length: 0 });
  };

  const handleDragOver = (e) => {
    if (state.blocked) return;
    e.preventDefault();
    e.stopPropagation();
  };

  const handleDrop = (e) => {
    if (state.blocked) return;
    e.preventDefault();
    e.stopPropagation();
    setCount(0);
    setState({ blocked: false, draggin: false, length: 0 });
    e.dataTransfer.files &&
      e.dataTransfer.files.length &&
      dropFileHandler(e.dataTransfer.files);
    props.handler && props.handler(e.dataTransfer.files);
  };

  const dropFileHandler = (files) => {
    for (let i = 0; i < files.length; i++) {
      let data = new FormData();
      const file = files[i];
      data.append("file", file);
      upload(data)
      .then(response => {
        if(response.data.status === 'error') {
          notify({
            status: 'error',
            title: "Файл не был загружен",
            body: response.data.message,
            delay: 3500
          });
        }
      })
      .catch((error) => {
        if (typeof error.response === typeof undefined) {
          networkError(error);
        } else {
          notify({
            status: 'error',
            title: "Файл не был загружен",
            body: error.response.data.message,
            delay: 3500
          });
        }
      });
      data = null;
    }
    // setUploading();
  };

  const handleStart = () => {
    setState({ blocked: true, draggin: false, length: 0 });
  };
  const handleEnd = () => {
    setState({ blocked: false, draggin: false, length: 0 });
  };

  useEffect(() => {
    setCount(0);
    const ref = dropRef.current;
    let div = ref;
    div.addEventListener("dragenter", handleDragEnter);
    div.addEventListener("dragleave", handleDragLeave);
    div.addEventListener("dragover", handleDragOver);
    div.addEventListener("drop", handleDrop);
    div.addEventListener("dragstart", handleStart);
    div.addEventListener("dragend", handleEnd);
    return () => {
      let div = ref;
      div.removeEventListener("dragenter", handleDragEnter);
      div.removeEventListener("dragleave", handleDragLeave);
      div.removeEventListener("dragover", handleDragOver);
      div.removeEventListener("drop", handleDrop);
      div.removeEventListener("dragstart", handleStart);
      div.removeEventListener("dragend", handleEnd);
    };
  }, [dropRef]);

  return (
    <div className={classNames("touchdot-uploader")} ref={dropRef}>
      {props.children}
      {state.draggin && (
        <div className="touchdot-uploader-overlay">
          <div className="touchdot-uploader-overlay-icon">
            <CloudUploadOutlined />
          </div>
          <div className="touchdot-uploader-overlay-caption">
            Загрузить файлы ({state.length} шт.)
          </div>
        </div>
      )}
    </div>
  );
};
const { networkError, notify } = notificationsActions;
const { upload } = resourcesActions;
export default connect(({ resources }) => resources, {
  upload,
  networkError,
  notify,
})(Drophandler);

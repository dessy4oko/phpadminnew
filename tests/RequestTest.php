<?php

class RequestTest extends \PHPUnit\Framework\TestCase
{
    private \App\Request $request;

    protected function setUp(): void
    {
        $this->request = new \App\Request();
    }

    /**
     * @covers \App\Request::headers
     */
    public function testRequestReturnsHeadersDriver() {
      $this->assertInstanceOf(\App\HeadersDriver::class, $this->request->headers());
    }

    /**
     * @covers \App\Request::forceProperty
     */
    public function testRequestCantCreateInvalidProperties() {
      $this->expectException(\Exception::class);
      $this->request->forceProperty("2", 1);
    }
    /**
     * @covers \App\Request::forceProperty
     */
    public function testRequestCreatesPropery() {
      $this->request->forceProperty('foo', 'foo');
      $this->assertObjectHasAttribute('foo', $this->request, "Объект запроса не создаёт свойства");
    }
    /**
     * @covers \App\Request::query
     */
    public function testRequestProvidesQueryParamsInterface() {
      $this->assertInstanceOf(\App\RequestQuery::class, $this->request->query(), 'Объект запроса не предоставляет интерфейс для работы с параметрами запроса');
    }

}

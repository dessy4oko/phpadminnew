<?php 
  use PHPUnit\Framework\TestCase;

  class EnumShell extends \App\Enum {
    public const get = 'get';
    // WARN: Do not declare `test` constant to provide tests work.
  }

  class EnumTest extends TestCase {

    /**
     * @coversNothing 
     */
    public function testEnumReturnsValue() {
      $this->assertSame('get', EnumShell::get, 'Перечисление не возвращает значение при прямом обращении к полю');
      $this->assertNotSame('test', EnumShell::get);
    }

    /**
     * @covers \App\Enum::has
     */
    public function testEnumHasValue() {
      $this->assertTrue(EnumShell::has('get'));
      $this->assertFalse(EnumShell::has('test'));
    }

    /**
     * @covers \App\Enum::fields
     */
    public function testEnumReturnsFieldsAsArray() {
      $this->assertIsArray(EnumShell::fields());
    }

    /**
     * @covers \App\Enum::fields
     */
    public function testEnumRetursFields() {
      $this->assertArrayHasKey('get', EnumShell::fields());
    }
  }
?>
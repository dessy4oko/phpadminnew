<?php
use PHPUnit\Framework\TestCase;

class PathTestShell extends \App\Path {
    public static function hasModificatorPublisher (string $path) : bool {
        return self:: hasModificator($path);
    }
    public static function modificatorPublisher (string $path) : string {
        return self::modificator($path);
    }
}

class PathTest extends TestCase
{

    protected function setUp(): void {
        \App\ConfigFabric::load(dirname(__DIR__).'/config.json');
    }
    /**
     * @covers \App\Path::root()
     */
    public function testBasePath()
    {
        $this->assertInstanceOf(\App\ResolvedPath::class, \App\Path::root());
        $this->assertSame(dirname(__DIR__), (string) \App\Path::root(), "Неверно работает получение корня сайта");
    }

    /**
     * @covers \App\Path::modificator
     */
    public function testModificatorFetchMethod() {
        $this->assertSame('~', PathTestShell::modificatorPublisher('~/index.php'));
        $this->assertSame('+', PathTestShell::modificatorPublisher('+/index.php'));
        $this->assertNotSame('#', PathTestShell::modificatorPublisher('~/index.php'));
    }

    /**
     * @covers \App\Path::hasModificator
     */
    public function testModificatorDetection() {
        $this->assertTrue(PathTestShell::hasModificatorPublisher('~/index.php'), 'Не верно работает определение модификатора корня');
        $this->assertTrue(PathTestShell::hasModificatorPublisher('+/index.php'), 'Не верно работает определение модификатора плагинов');
        $this->assertFalse(PathTestShell::hasModificatorPublisher('#/index.php'), 'Не верно работает определение модификатора при отсутствующем модификаторе');
    }

    /**
     * @covers \App\Path::resolve
     */
    public function testRootPathResolve()
    {
        $this->assertInstanceOf(\App\ResolvedPath::class,\App\Path::resolve('~/index.php'), "Метод должен возвращать объект для поддержки цепочек вызовов");
        $this->assertNotSame(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'index.php', \App\Path::resolve('~/index.php'), "Разбор пути не должен возвращать строку для поддержки цепочек вызовов");
        $this->assertSame(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'index.php', (string)\App\Path::resolve('~/index.php'), "Неверное преобразование путей");
        $this->assertTrue(file_exists(\App\Path::resolve('~/index.php')));
    }

    /**
     * @covers \App\Path::resolve
     */
    public function testPluginPathResolve()
    {
        $this->assertSame(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'infusions' . DIRECTORY_SEPARATOR . 'index.php', (string)\App\Path::resolve('+/index.php'));
        $this->assertTrue(file_exists(\App\Path::resolve('+/index.php')));
    }

    /**
     * @depends testBasePath
     * @covers \App\ResolvedPath::join
     */
    public function testJoinMethod() {
        $this->assertInstanceOf(\App\ResolvedPath::class, \App\Path::root()->join(), 'Неверный тип возвращаемого объекта после объединени пути без аргументов');
        $this->assertInstanceOf(\App\ResolvedPath::class, \App\Path::root()->join('infusions'), 'Неверный тип возвращаемого объекта после объединения пути с одним аргументом');
        $this->assertInstanceOf(\App\ResolvedPath::class, \App\Path::root()->join('infusions', 'index.php'), 'Неверный тип возвращаемого объекта после объединения пути с несколькими аргументами');
        $this->assertInstanceOf(\App\ResolvedPath::class, \App\Path::root()->join('infusions')->join('index.php'), 'Неверный тип возвращаемого объекта после сцепленного объединения пути');
        $this->assertSame((string)\App\Path::root(), (string)\App\Path::root()->join(), 'Пустое объединение не должно модифицировать путь');
        $this->assertSame(
            dirname(__DIR__) . DIRECTORY_SEPARATOR . 'infusions', 
            (string) \App\Path::root()->join('infusions'), 
            'Неверная модификация пути после объединения c одним аргументом'
        );
        $this->assertSame(
            dirname(__DIR__) . DIRECTORY_SEPARATOR . 'infusions' . DIRECTORY_SEPARATOR . 'infusions', 
            (string) \App\Path::root()->join('infusions', 'infusions'),
            'Неверная модификация пути после объединения с двумя аргументами'
        );
        $this->assertSame(
            dirname(__DIR__) . DIRECTORY_SEPARATOR . 'infusions' . DIRECTORY_SEPARATOR . 'infusions', 
            (string) \App\Path::root()->join('infusions')->join('infusions'),
            'Неверная модификация пути после сцепленного объединения'
        );
    }
    /**
     * @covers \App\ResolvedPath::webpath
     */
    public function testWebPathFormat() {
        $this->assertSame("/infusions/image.png", \App\Path::root()->join('infusions', 'image.png')->webpath());
        $this->assertSame("/infusions/", \App\Path::resolve("~/infusions/")->webpath());
        $this->assertSame("/infusions/", \App\Path::root()->join('infusions')->webpath());
        $this->assertSame("/infusions/index.php", \App\Path::root()->join('infusions', 'index.php')->webpath());
        $this->assertSame("/infusions/index.php", \App\Path::resolve("+/index.php")->webpath());
    }

    /**
     * @covers \App\ResolvedPath::join
     */
    public function testSlashDoesNotModifyPath() {
        $this->assertSame("/infusions/", \App\Path::root()->join('infusions')->join('/')->webpath());
    }

    /**
     * @covers \App\Resolvedpath::resolve
     */
    public function testResolvedPathResolves() {
        $this->assertSame('/infusions/image.png', \App\Path::resolve("~/infusions")->join('/')->join('/image.png')->resolve()->webpath());
    }
}

<?php 
    use PHPUnit\Framework\TestCase;
    require_once dirname(__DIR__) . '/core/config/Config.php';

    class TestedConfigInstance extends \App\Config {
        public function getProps() {
            return $this->props;
        }
    }

    class ConfigTest extends TestCase {

        private object $props;

        protected function setUp():void {
            $this->props = (object)[
                "encoding" => 'utf-8'
            ];
        }

        /**
         * @covers \App\Config::__construct
         */
        public function testConstructorFailsOnEmptyConfigObject() {
            $this->expectException(\App\EmptyConfigObject::class);
            new \App\Config((object)[]);
        }

        /**
         * @covers \App\Config::__construct
         */
        public function testInstanceStoresConfigObject() {
            $configInstance = new TestedConfigInstance($this->props);
            $this->assertSame($this->props, $configInstance->getProps());
            return $configInstance;
        }

        /**
         * @depends testInstanceStoresConfigObject
         * @covers \App\Config::__get
         */
        public function testInstanceReturnsProperty(\App\Config $configInstance) {
            $this->assertSame('utf-8', $configInstance->encoding);
        }

        /**
         * @depends testInstanceStoresConfigObject
         * @covers \App\Config::__get
         */
        public function testInstancePropertyException(\App\Config $configInstance) {
            $this->expectException(\App\ConfigPropertyApsend::class);
            $configInstance->foo;
        }
    }
?>
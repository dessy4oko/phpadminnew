<?php
use App\AllowedUsageTypes;
use App\Controller;
use App\MiddleWare;
use App\MiddleWareSet;
use App\Request;
use App\Response;
use Main\MainController;
use PHPUnit\Framework\TestCase;


class CustomMiddleWare extends \App\MiddleWare
{
    public function __invoke(Request &$request, Response &$response)
    {
        
    }
}

class NotAllowed
{}

class CustomController extends \App\Controller
{
    public function init(): void
    {
        
    }
}

class CustomNestedController extends CustomController {

}

class AllowedUsageTypesTest extends TestCase
{

    /**
     * @covers \App\AllowedUsageTypes::allowed
     */
    public function testAllowedTypes()
    {
        $this->assertTrue(AllowedUsageTypes::allowed(new CustomMiddleWare()), "Множество должно пропускать дочерние объекты класса MiddleWare");
        $this->assertTrue(AllowedUsageTypes::allowed(new MiddleWareSet()), "Множество должно пропускать объекты класса MiddleWareSet");
        $this->assertFalse(AllowedUsageTypes::allowed(new NotAllowed()), "Множество не должно пропускать недопустимые объекты");
        $this->assertFalse(AllowedUsageTypes::allowed(new ArrayObject()), "Множество не должно пропускать недопустимые объекты");
        $this->assertTrue(AllowedUsageTypes::allowed(new CustomController()), "Множество должно пропускать объекты класса Controller");
        // Real controllers
        $controller = new CustomNestedController();
        $this->assertTrue(AllowedUsageTypes::allowed($controller), "Множество должно пропускать объекты класса, унаследованного от Controller");
    }
}

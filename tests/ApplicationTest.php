<?php

use App\MiddleWare;
use App\Request;
use App\Response;
use PHPUnit\Framework\TestCase;

class CustomMiddleWareShell extends MiddleWare
{
    public function __invoke(Request &$request, Response &$response)
    {
        
    }
}

class ApplicationShell extends \App\Application {
    public function getMiddleware() {
        return $this->middleware;
    }

    public function getControllers() {
        return $this->controllers;
    }
    public static function instance()
    {
        
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}

class ApplicationTest extends TestCase
{

    /**
     * @covers \App\Application::create
     */
    public function testInstanceReturned()
    {
        $this->assertInstanceOf(\App\Application::class, \App\Application::create());
        $this->assertInstanceOf(\App\Application::class, \App\Application::instance());
        $this->assertSame(\App\Application::instance(), \App\Application::create());

        return \App\Application::instance();
    }

    /**
     * @depends testInstanceReturned
     * @covers \App\Application::use
     */
    public function testApplicationThrowsNotAllowedType(\App\Application $app)
    {
        $this->expectException(\App\NotAllowedType::class);
        $app->use(new ArrayObject());
    }

}

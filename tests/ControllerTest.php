<?php 
  use PHPUnit\Framework\TestCase;

class ControllerTestInstance extends \App\Controller {
  public function init(): void
  {
    
  }
  public function getRoutes() {
    return $this->routes;
  }
  public function canProcess($method) {
    return parent::canProcess($method);
  }
}

  class ControllerTest extends TestCase {

    /**
     * @covers \App\Controller::route
     */
    public function testControllerStoresRoutes() {
      $controller = new ControllerTestInstance();
      $controller->route(\App\RequestMethods::get, "/^\/help\/$/", new \App\RequestHandler(function() {
        return 'Hello';
      }));

      $this->assertNotEmpty($controller->getRoutes());

      return $controller;
    }

    /**
     * @depends testControllerStoresRoutes
     * @covers \App\Controller::canProcess
     */
    public function testControllerResolvesMethods(ControllerTestInstance $controller) {
      $this->assertTrue($controller->canProcess('GET'));
    }

    /**
     * @depends testControllerStoresRoutes
     * @covers \App\Controller::__invoke
     */
    public function testControllerExecutesMatchingRoute(ControllerTestInstance $controller) {
      // * Testing request setup
      $request = new \App\Request();
      $request->url = '/help/';
      $request->rewritableUrl = '/help/';
      $request->method = 'GET';
      // * Testing response setup
      $response = new \App\Response();
      $this->assertTrue($controller($request, $response));
      $request->method = 'POST';
      $this->assertFalse($controller($request, $response));
      $request->url = '/some/route/path/';
      $request->rewritableUrl = '/some/route/path/';
      $request->method = "GET";
      $this->assertFalse($controller($request, $response));
    }
  }
?>
<?php 

class RequestQueryTest extends \PHPUnit\Framework\TestCase {
  protected \App\RequestQuery $query;

  protected function setUp(): void
  {
    $_GET['someparam'] = 'somevalue';
    $this->query = new \App\RequestQuery();
  }
  /**
   * @covers \App\RequestQuery::has
   */
  public function testRequestQueryChecksParamIssetRight() {
    $this->assertTrue($this->query->has('foo'));
  }

  /**
   * @covers \App\RequestQuery::has
   */
  public function testRequestQueryChecksParamNotIssetRight() {
    $this->assertFalse($this->query->has('23'));
  }

  /**
   * @covers \App\RequestQuery::all
   */
  public function testRequestQueryReturnsAllParams() {
    $this->assertIsArray($this->query->all());
    $this->assertArrayHasKey('someparam', $this->query->all());
  }

  /**
   * @covers \App\RequestQuery::__get
   */
  public function testRequestQueryReturnsPropertyParam() {
    $this->assertSame('somevalue', $this->query->someparam);
  }

}

?>
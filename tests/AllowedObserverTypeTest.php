<?php 
    use PHPUnit\Framework\TestCase;

    class ObserverVariant {
        use \App\Observer;
    }

    class ObserverNextVariant extends ObserverVariant {}

    class AllowedObserverTypeTest extends TestCase {
        /**
         * @covers \App\AllowedObserverType::allowed
         */
        public function testAllowedTypes() {
            
            $this->assertTrue(\App\AllowedObserverType::allowed(new ObserverVariant()), "Проверка должна пропускать корневой Observer");
            $this->assertTrue(\App\AllowedObserverType::allowed(new ObserverNextVariant()), "Проверка должна пропускать корневой Observer");
        }
    }
?>
<?php

use App\Request;
use App\RequestHandler;
use App\Response;

class RequestHandlerShell extends RequestHandler {
  public function getCallback() {
    return $this->callback;
  }
}

class RequestHandlerTest extends \PhpUnit\Framework\TestCase {
  protected \App\Request $request;
  protected \App\Response $response;
  protected function setUp():void {
    $this->request = new \App\Request();
    $this->response = new \App\Response();
  }
  /**
   * @covers \App\RequestHandler::__construct
   */
  public function testRequestHandlerStoresCallback() {
    $handler = new RequestHandlerShell(function() {});
    $this->assertIsCallable($handler->getCallback(), "Обработчик запроса не сохраняет колбэк.");
    return $handler;
  }
}
?>
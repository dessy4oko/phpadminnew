<?php

use PHPUnit\Framework\TestCase;
require_once dirname(__DIR__) . '/core/config/ConfigFabric.php';

class ConfigFabricTestShell extends \App\ConfigFabric {
    /**
     * @return \App\Config[];
     */
    public static function getConfigs(): array {
        return self::$config;
    }
    public static function flush(): void {
        self::$defaultKey = '';
        self::$config = [];
    }
}

final class ConfigFabricTest extends TestCase
{


    // ! Tests
    /**
     * @covers \App\ConfigFabric::load 
     */
    public function testLoadException()
    {
        $this->expectException(\App\ConfigLoadException::class);
        \App\ConfigFabric::load('');
    }
    
    /**
     * @covers \App\ConfigFabric::store 
     */
    public function testConfigStoresDefaultObject() {
        ConfigFabricTestShell::flush();
        ConfigFabricTestShell::store((object)['encoding']);
        $this->assertNotEmpty(ConfigFabricTestShell::getConfigs(), "Метод выпуска нового объекта конфигурации не сохраняет объект");
        $this->assertArrayHasKey('default', ConfigFabricTestShell::getConfigs(), "Неверно записывается объект по умолчанию при первом присваивании");
        $this->assertInstanceOf(\App\Config::class, ConfigFabricTestShell::get(), "Неверно выпускается объект конфигурации");
    }

    /**
     * @covers \App\ConfigFabric::store
     */
    public function testConfigStoresObjectByKey() {
        ConfigFabricTestShell::flush();
        ConfigFabricTestShell::store((object)['encoding'], 'test');
        $this->assertArrayHasKey('test', ConfigFabricTestShell::getConfigs(), "Неверно записывается объект по ключу");
        $this->assertSame('test', ConfigFabricTestShell::getDefaultKey(), "Неверно присваивается ключ по умолчанию при указании ключа");
        $this->assertInstanceOf(\App\Config::class, ConfigFabricTestShell::get('test'), "Неверно возвращается объект конфигурации по ключу");
    }

    /**
     * @covers \App\ConfigFabric::load
     */
    public function testFirstLoadKey()
    {
        ConfigFabricTestShell::flush();
        ConfigFabricTestShell::load(dirname(__FILE__) . '/testconfig.json');
        $this->assertSame('default', ConfigFabricTestShell::getDefaultKey(), "При первой загрузке без указания ключа неверно присваивается ключ по умолчанию");
        return \App\ConfigFabric::get();
    }

    /**
     * @depends testFirstLoadKey
     * @covers \App\ConfigFabric::get
     */
    public function testConfigReturnsObjectNotSpecifiedKey($config) {
        $this->assertInstanceOf(\App\Config::class, $config, "Неверно возвращается объект конфигурации без указания его ключа");
    }

    /**
     * @covers \App\ConfigFabric::load
     */
    public function testLoadByKey() {
        ConfigFabricTestShell::flush();
        ConfigFabricTestShell::load(dirname(__FILE__) . '/testconfig.json', 'test');
        $this->assertFalse(empty(ConfigFabricTestShell::getConfigs()), "Метод выпуска нового объекта по ключу конфигурации не сохраняет объект");
        $this->assertArrayHasKey('test', ConfigFabricTestShell::getConfigs(), "Неверно записывается объект по ключу");
        $this->assertInstanceOf(\App\Config::class, ConfigFabricTestShell::get(), "Неверно выпускается объект конфигурации");
    }
}

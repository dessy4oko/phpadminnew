<?php
namespace Admin;

use App\Request;
use App\RequestHandler;
use App\Response;

class AdminService
{
    public static function dashboard(): RequestHandler
    {
        return new RequestHandler(function (Request $request, Response $response) {
          $response->view('administration/index.twig', [])->end();
        });
    }
}

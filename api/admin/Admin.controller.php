<?php
namespace Admin;
require_once \App\Path::resolve("./Admin.service.php");
require_once \App\Path::resolve("~/api/middleware/Admin.guard.php");

use App\Controller;
use App\MiddleWareSet;

class AdminController extends Controller
{
    public function init(): void
    {
        $this->get("/^\/dashboard\/$/", AdminService::dashboard(), new MiddleWareSet([new \Admin\Guard()]));
    }
}

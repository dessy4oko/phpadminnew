<?php

use App\Request;
use App\Response;

/**
 * Edit the CORS field in your configuration file of applicaiton.
 * 
 * This middleware expects the same structure as followed: 
 * 
 * "Origin": " * | none | https://example.com " 
 * 
 * "Methods": "POST, HEAD, GET, PUT, PATCH, DELETE, OPTIONS" 
 * 
 * "Headers" : "*headers to allow*"
 * 
 * If there is no Origin field in your configuration file then response headers won`t be changed
 * @param string $configKey Key of config in \App\ConfigFabric to use. If not specified then default config will be used.
 * @throws \App\InvalidConfigKey
 */
class AllowCORS extends \App\MiddleWare {
    protected ?string $key = null;

    public function __construct(string $configKey = null)
    {
        if(!is_null($configKey) && !\App\ConfigFabric::has($configKey)) {
            throw new \App\InvalidConfigKey($configKey, "Can not resolve config object by passed key");
        }
        $this->key = $configKey;
    }

    public function __invoke(Request &$request, Response &$response)
    {
        $cors = \App\ConfigFabric::get($this->key)->CORS;
       

        $fields = array_keys(get_object_vars($cors));

        if(in_array('Origin', $fields)) {
            $response->headers()->set("Access-Control-Allow-Origin", $cors->Origin);
        }

        if(in_array('Methods', $fields)) {
            $response->headers()->set("Access-Control-Allow-Methods", $cors->Methods);
        }

        if(in_array('Headers', $fields)) {
            $response->headers()->set("Access-Control-Allow-Headers", $cors->Headers);
        }

    }
}
<?php 
use App\MiddleWare;
use App\Path;
use App\Request;
use App\Response;

class Logger extends MiddleWare {
  protected $callback = null;
  public function __construct(callable $callback)
  {
    $this->callback = $callback;
  }

  public function __invoke(Request &$request, Response &$response)
  {
    if(!is_dir(Path::resolve(\App\ConfigFabric::get()->log))) {
      mkdir(Path::resolve(\App\ConfigFabric::get()->log));
    }
    $filename = Path::resolve(\App\ConfigFabric::get()->log)->join((new DateTime('NOW'))->format('d.m.Y').'.txt');
    $fhandler = fopen($filename, "a+");
    if(!$fhandler) $response->end('Failed to open file with filename: '.$filename);
    fwrite($fhandler, ($this->callback)($request, $response));
    fclose($fhandler);
  }
}
?>
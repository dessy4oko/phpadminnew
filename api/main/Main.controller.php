<?php 
namespace Main;

use App\BodyParser;
use App\BodyParserMode;
use App\Controller;
use App\MiddleWareSet;
use App\Path;
use App\Request;
use App\Response;
use App\RequestMethods;

require_once Path::resolve('./Main.service.php'); 

class MainController extends Controller {
  public function init():void {
    
    $this->get("/^\/$/", MainService::sayHello());

  }
}

?>
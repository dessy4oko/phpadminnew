<?php
namespace Main;

use App\BodyParserMode;
use App\Request;
use App\RequestHandler;
use App\Response;
use App\db;
require_once \App\Path::resolve("~/api/middleware/Logger.middleware.php");
abstract class MainService {
    public static function sayHello() {
      $handler = new RequestHandler(function(Request &$req, Response &$res) {
        $result = db::$connection->query('SELECT * FROM `fusion5vn4q_settings` LIMIT 1');
        $res->view('sayhello.twig', ['title' => 'Page successfully Found', 'result' => $result->fetchObject()])->end();
      });
      return $handler;
    }
  }
?>
<?php
namespace Resources;

use App\Controller;
use App\MiddleWareSet;
use App\Path;
use App\Request;
use App\RequestMethods;
use App\Response;
use App\BodyParserMode;
use App\BodyParser;
use Logger;

require_once Path::resolve('./Resources.service.php');
require_once Path::resolve('~/api/middleware/Logger.middleware.php');

class ResourcesController extends Controller
{
    public function init(): void
    {
        // CORS OPTIONS
        $this->route(RequestMethods::options, "/^\/resources\/$/", ResourcesService::allowCORS(), new MiddleWareSet([new \Logger(function (Request &$req, Response $res) {
            return "Options sent\n";
        })]));
        // File upload process
        $this->post("/^\/resources\/$/", ResourcesService::upload(),new MiddleWareSet([new BodyParser(BodyParserMode::join(BodyParserMode::files, BodyParserMode::multipart)), new \Logger(function (Request &$req, Response $res) {
            return json_encode($req->files)."\n";
        }), new \App\db(\App\ConfigFabric::get()->pdo)]));
        // Returns list of stored files
        $this->get("/^\/resources\/$/", ResourcesService::list(), new MiddleWareSet([new BodyParser(BodyParserMode::multipart)]));
        // Delete files
        $this->delete("/^\/resources\/$/", ResourcesService::deleteFiles(), new MiddleWareSet([new BodyParser(BodyParserMode::json), new \App\db(\App\ConfigFabric::get()->pdo)]));
        // Create directory
        $this->put("/^\/resources\/$/", ResourcesService::makeDir(), new MiddleWareSet([new BodyParser(BodyParserMode::json), new \App\db(\App\ConfigFabric::get()->pdo), new Logger(function(Request &$req, Response &$res) {
            return "Body:".json_encode($req->body)."\n";
        })]));
    }
}

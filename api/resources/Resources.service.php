<?php

namespace Resources;

use App\Path;
use App\Request;
use App\RequestHandler;
use App\Response;

abstract class ResourcesService
{
    public static function allowCORS()
    {
        return new RequestHandler(function (Request &$req, Response &$res) {
            $res->end();
        });
    }
    public static function upload()
    {
        return new RequestHandler(function (Request &$req, Response &$res) {

            try {
                // ? If there is no files info in request object
                if (empty($req->files)) {
                  // ! THROW Exception
                  // ! because there is nothing to process
                  throw new \Exception('Files not found.');
                }
                // * For each file which contains in request object
                foreach ($req->files as $file) {
                  // ? Check each temp file object to be loaded via POST query
                  if (!$file->isuploaded) {
                      continue;
                  }

                  $fObj = \App\FilesFabric::produce($file->type, $file->tmp_name, $file->size);

                  // * 1.1 Rename temp file with uploading filename
                  if (!$fObj->rename($file->name)) {
                    // ! THROW Exception
                    // ! because rename process have failed
                    throw new \Exception("Failed to rename uploaded file. Current name is `{$fObj->name()}`. Target name is `{$file->name}`");
                  }

                  // TODO: 1. Обработать файл в зависимости от его типа
                  //       *  ужать изображения до необходимых размеров, создать легковесные версии изображений
                  //       *  проверить svg файлы, вырезать лишние атрибуты, комментарии
                  //       *  видео-файлы сконвертировать в недостающие форматы, сделать превью

                  // * 1.2 Move file to constant location
                  if (!is_dir(Path::resolve("~/public/")) && function_exists('mkdir')) {
                      mkdir(Path::resolve("~/public/"));
                  }
                  // * Resolve target folder
                  $targetFolder = Path::resolve("~/public/");

                  // * Move file to target folder
                  if (!$fObj->moveTo($targetFolder)) {
                      // ! THROW Exception
                      // ! because moving process have failed
                      throw new \Exception("Failed to move uploaded file `{$fObj->name()}` to target folder `{$targetFolder}`");
                  }

                  $id = uniqid();
                  // * Store data in database
                  $result = \App\db::$connection
                      ->prepare("INSERT INTO `resources` SET `id`=:id, `name`=:name, `parent`=:parent, size=:size, mimetype=:mimetype")
                      ->execute([
                          'id' => $id,
                          'name' => $fObj->name(),
                          'parent' => '/',
                          'size' => $fObj->size(),
                          'mimetype' => $fObj->type(),
                      ]);

                  /**
                   * ! Delete uploaded file if database indexing process fails
                   */
                  if (!$result) {
                      $fObj->delete();

                      throw new \Exception('Не удалось сохранить файл. Возможно файл с таким именем уже существует в целевой директории.');
                  }
                }
                // * Send result
                $res->json(['uid' => $id, 'name' => $fObj->name(), 'size' => $fObj->size()])->end();
            } catch (\App\NotAllowedType $e) {
                $res->headers()->setCode(\App\httpCode::badrequest);
                $res->json(['message' => 'Not allowed type (' . $e->passedType() . ') of file recieved.', 'status' => 'error'])->end();
            } catch (\Exception $e) {
                $res->json(['message' => $e->getMessage(), 'status' => 'error'])->end();
            }

            $res->end(serialize($req->files));
            $file = $_FILES['file'];
            $file['uid'] = rand();
            unset($file['type']);

            $res->json($file)->end();
        });
    }

    public static function list() {
        return new RequestHandler(function (Request &$req, Response &$res) {
            $filesList = [];
            // * Get information from database
            $result = \App\db::$connection->query("SELECT * FROM `resources`");
            // * Prepare information for client
            foreach ($result as $fileInfo) {
                $filesList[] = [
                    'name' => $fileInfo['name'],
                    'size' => (int) $fileInfo['size'],
                    'uid' => $fileInfo['id'],
                    'parent' => $fileInfo['parent'],
                    'mimetype' => $fileInfo['mimetype']
                ];
            }
            // * Send
            $res->json($filesList)->end();
        });
    }
    public static function deleteFiles()
    {
        return new RequestHandler(function (Request &$req, Response &$res) {

            $files = [];
            $removed = [];
            try {
                // ? Check that request contains required data to process this handler
                if (
                    empty($req->body)
                    or !is_object($req->body)
                    or !property_exists($req->body, 'resources')
                    or empty($req->body->resources)
                ) {
                    // ! THROW Exception
                    // ! because there is no resources to delete
                    throw new \Exception('Не получены идентификаторы ресурсов для удаления');
                }

                // * Prepare database query
                $query = \App\db::$connection
                    ->prepare("SELECT `id`,`name`, `size`, `mimetype` AS `type`, `parent` FROM `resources` WHERE `id` IN (:ids) LIMIT :length");
                // ? If query preparation fails
                if (!$query) {
                    // ! THROW Exception
                    // ! because query can not be executed
                    throw new \Exception('Произошла ошибка при подготовке запроса');
                }

                // * Prepare data for database query
                $length = count($req->body->resources);
                $ids = implode(',', array_map(function ($id) {
                    return "{$id}";
                }, $req->body->resources));

                $query->bindParam(':ids', $ids, \PDO::PARAM_STR);
                $query->bindValue(':length', $length, \PDO::PARAM_INT);

                // * Get files information
                $result = $query->execute();

                // ? If nothing returned from database query
                if (!$query->rowCount() || !$result) {
                    // ! THROW Exception
                    // ! because the files with the passed identificators do not exist
                    throw new \Exception('Файлы с указанными идентификаторами не были найдены');
                }
                // Start transaction
                \App\db::$connection->beginTransaction();
                foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $file) {
                    $file = (object) $file;
                    // TODO: remove folder
                    // * Resolve system filepath
                    $path = Path::resolve('~/public/')->join($file->parent)->join($file->name)->resolve();

                    // * Trying to create File instance
                    $fObj = \App\FilesFabric::produce($file->type, $path, $file->size);

                    // * Remove file from database
                    $result = \App\db::$connection->prepare("DELETE FROM `resources` WHERE `id`= :id")->execute(['id' => $file->id]);

                    // ? If deletion operation fails
                    if (!$result) {
                        // * Rollback transaction
                        \App\db::$connection->rollBack();
                        // ! THROW Exception
                        throw new \Exception('Операция завершена не полностью. Не удалось удалить файл ' . $fObj->path(), 8000);
                    }
                    // * and store removed file identificator
                    $files[] = $fObj;
                    $removed[] = $file->id;
                }

                foreach ($files as $fObj) {
                    // * Remove file from storage
                    $fObj->delete();
                }
                // Commit changes in database
                \App\db::$connection->commit();

            } catch (\App\NotAllowedType $e) {
                // * Rollback transaction
                \App\db::$connection->rollBack();
                // TODO: Решить, что делать с теми файлами, типы которых не поддерживаются системой, но в базе данных они записаны
                $res->json(['status' => 'error', 'message' => $e->getMessage()])->end();
            } catch (\Exception $e) {
                switch ($e->getCode()) {
                  case 8000:
                    $res->json(['status' => 'warning', 'message' => $e->getMessage(), 'removed' => $removed])->end();
                    break;
                  default:
                    $res->json(['status' => 'error', 'message' => $e->getMessage(), 'removed' => $removed])->end();
                    break;
                }
            }
            $res->json(['satus' => 'success', 'message' => 'Файлы успешно удалены', 'removed' => $removed])->end();
        });
    }

    public static function makeDir()
    {
        return new RequestHandler(function (Request &$req, Response &$res) {
            $folderInfo = [];
            try {
                if(empty($req->body->dirname)) {
                    throw new \Exception('Невозможно создать папку. У папки должно быть имя. Папка без имени - это не папка.');
                }
                if(!isset($req->body->parent)) {
                    $req->body->parent = '/';
                }
                if(!is_dir($req->body->parent)) {
                    throw new \Exception('Родительский каталог не существует.');
                }
                $path = Path::resolve('~/public')->join($req->body->parent);
                $path = \App\Storage::makefolder($req->body->dirname, $path);
                
                $id = uniqid();
                // * Store data in database
                $result = \App\db::$connection
                    ->prepare("INSERT INTO `resources` SET `id`=:id, `name`=:name, `parent`=:parent, size=:size, mimetype=:mimetype")
                    ->execute([
                        'id' => $id,
                        'name' => basename($path),
                        'parent' => '/',
                        'size' => 0,
                        'mimetype' => 'folder',
                    ]);
                $folderInfo['id'] = $id;
                $folderInfo['mimetype'] = 'folder';
                $folderInfo['name'] = basename($path);
                $folderInfo['size'] = 0;
                $folderInfo['parent'] = Path::resolve(preg_replace("/".basename($path)."$/", '', $path))->webpath();

            } catch (\Throwable $e) {
                $res->json(['status' => 'error', 'message' => $e->getMessage()])->end();
            } finally {
                if(empty($folderInfo)) {
                    $res->json(['status' => 'error', 'message' => 'Unexpected error ocured'])->end();
                }
            }
            $res->json(['status' => 'success', 'message' => 'Папка успешно создана', 'folder' => $folderInfo])->end();
        });
    }
}
